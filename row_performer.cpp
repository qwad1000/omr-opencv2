#include "row_performer.h"

row_performer::row_performer(Mat &source_image, Mat &destufed_image,
                             const staff_properties &prop)
    : _source_image(source_image), _destuffed_image(destufed_image),
      _prop(prop)
{
}

void row_performer::perform()
{    
    _pre_rows = find_rows(_destuffed_image, _prop);
    _row_rois = findRowROIs();

    //viewer::viewMat("asd0", viewer::viewRects(_source_image, rects, Scalar(255,0,0),3));

    for(uint i=0; i<_row_rois.size(); i++) {
        deskewer::deskew_rect(_destuffed_image, _prop, _row_rois[i], _source_image);
    }


    double rle_delsize = 1.5*_prop.staffline_height;
    int ceil_size = ceil(rle_delsize);
    cout<<"ceil"<<ceil_size<<"  "<<rle_delsize<<endl
       <<ceil(0.5*_prop.staffline_height)<<endl;
    runlength::filter_vertical_long_runs(_destuffed_image, ceil_size , false);

    runlength::filter_horizontal_short_runs(_destuffed_image, ceil(0.5*_prop.staffline_height)+1, false);


    _pre_rows = find_rows(_destuffed_image,_prop);
    //deleteBetweenStaffLines(2);
    //TODO:!!!!
    //viewer::viewMat("mat", _destuffed_image);

    deleteAllButStuff();
    //viewer::viewMat("mat", _destuffed_image);
    cerr<<"end row-performer"<<endl;
}

vector<staff_row> row_performer::find_rows(const Mat &deskewed_mat, const staff_properties &staff_prop)
{
    vector<staff_row> y_rows = find_rows_y(deskewed_mat, staff_prop);
    find_rows_x(y_rows);

    vector<Rect> rectvector;
    for(uint i=0; i<y_rows.size(); i++) {
        rectvector.push_back(y_rows[i].rect);
    }

    //viewer::viewMat("row_rectangles", viewer::viewRects(_source_image,rectvector, Scalar (255,5,0), 1));

    return y_rows;
}

vector<staff_row> row_performer::find_rows_y(const Mat &destuffed, const staff_properties &staff_prop)
{
    int cols = destuffed.cols,
        rows = destuffed.rows;
    vector<int> black_y_projection = mat_helper::y_projection(destuffed, Range(0,cols), Range(0,rows));

    mask mas(staff_prop);
    vector<int> masked_projection;
    for(uint i=0; i<black_y_projection.size()-staff_prop.staffline_height; i++){
        int value = mas.staff_line(black_y_projection, i);
        masked_projection.push_back(value);
    }
    int maxElement = *max_element(black_y_projection.begin(), black_y_projection.end());

    vector<int> derivate_masked_r = vector_helper::right_derivate(masked_projection);

    vector<staff_row> staff_rows;
    staff_row temp_row;
    bool row_started = false;
    int j = 0;

    for(uint i=1; i<black_y_projection.size(); i++) {
        int dvalue = derivate_masked_r[i];
        if(dvalue < 0 && derivate_masked_r[i-1]>=0){
            int pr_value = black_y_projection[i];
            if(pr_value > maxElement/4.0){    ///todo: change 7 value to smth more complicated
                temp_row.lines.push_back(i);
                row_started = true;
                j = 0;
            }
        }
        if(row_started){
            j++;
        }
        if(j > 2*staff_prop.staffspace_height + 2.5*staff_prop.staffline_height){ ///test
            sort(temp_row.lines.begin(), temp_row.lines.end());
            /*if(temp_row.lines.size() != 5){
                cerr<<"not 5 lines detected";
            }*/
            int x=0;
            int y=temp_row.lines.front();
            int width = _source_image.cols;
            int height = temp_row.lines.back() - y + staff_prop.staffline_height;

            temp_row.rect = Rect(x,y,width, height);

            staff_rows.push_back(temp_row);
            temp_row.lines.clear();

            row_started = false;
            j = 0;
        }
    }

    /*vector<Rect> rectvector;
    for(uint i=0; i<staff_rows.size(); i++) {
        temp_row = staff_rows[i];
        for(uint j=0; j<temp_row.lines.size(); j++){
            rectvector.push_back(Rect(110, temp_row.lines[j],20,staff_prop.staffline_height));
            rectvector.push_back(Rect(_source_image.cols - 130, temp_row.lines[j],20,staff_prop.staffline_height));
            rectvector.push_back(Rect(_source_image.cols/2, temp_row.lines[j],20,staff_prop.staffline_height));
        }
        rectvector.push_back(temp_row.rect);
    }*/

   //viewer::viewMat("row_rectangles", viewer::viewRects(_source_image,rectvector, Scalar (255,5,0), 1));

    return staff_rows;
}

///протестировать на строке со многими подстроками.
void row_performer::find_rows_x(vector<staff_row> &rows_y) const
{
    for(staff_row &row : rows_y){
        Rect &row_rect = row.rect;

        Mat mat_to_x_proj = _source_image;
        vector<int> x_projection = mat_helper::x_projection(mat_to_x_proj, row_rect);

        vector<Range> black_ranges = vector_helper::getRanges(x_projection);

        const int deleteSize = 12; //todo
        auto pend_new = remove_if(black_ranges.begin(), black_ranges.end(),
                                  [deleteSize](const Range &range)
        {
            return range.size() < deleteSize; //todo: not constant
        });

        black_ranges.resize(pend_new - black_ranges.begin());

        sort(black_ranges.begin(), black_ranges.end(), [](const Range &r1, const Range &r2){
            return r1.size() > r2.size();
        });

        const int staff_in_row = 3;
        const float min_row_size = _source_image.cols / staff_in_row;
        pend_new = remove_if(black_ranges.begin(), black_ranges.end(),
                                  [min_row_size](const Range &range)
        {
            return range.size() < min_row_size; //todo: not constant
        });

        //todo: handle if two or more staffs is on the row

        Range range = black_ranges.front();
        row_rect.x = range.start;
        row_rect.width = range.size();
    }
}

//TODO: придумать более сложный и правильный алгоритм выбора областей строк. Этот дает сбитый текст в некоторых случаях.
vector<Rect> row_performer::findRowROIs()
{
    vector <Rect> rects;
    rects.push_back(Rect(0,0,_source_image.cols, (_pre_rows[1].rect.tl().y + _pre_rows[0].rect.br().y)/2));

    for(uint i=1; i<_pre_rows.size()-1; i++){
        Point tl = _pre_rows[i].rect.tl();
        Point br = _pre_rows[i].rect.br();

        Point prev_br = _pre_rows[i-1].rect.br();
        Point next_tl = _pre_rows[i+1].rect.tl();

        Point new_tl(0, (tl.y + prev_br.y)/2);
        Point new_br(_source_image.cols, (br.y + next_tl.y)/2);

        rects.push_back(Rect(new_tl, new_br));
    }

    rects.push_back(Rect(rects.back().br(), Point(0, _source_image.rows)));

    return rects;
}

void row_performer::deleteBetweenStaffLines(const int vmargin)
{
    for(uint i=0; i<_pre_rows.size(); i++){
        staff_row &row = _pre_rows[i];

        int x1 = row.rect.tl().x;
        int x2 = row.rect.br().x;

        rectangle(_destuffed_image, Point(x1, row.rect.tl().y - vmargin),
                  Point(x2, row.rect.tl().y - _prop.staffspace_height + vmargin),Scalar(255), -1);

        for(uint j=1; j<row.lines.size(); j++){
            int top = row.lines[j-1] + _prop.staffline_height;
            int bottom = row.lines[j];

            top += vmargin;
            bottom -= vmargin;

            Point tl(x1, top),
                  br(x2, bottom);

            rectangle(_destuffed_image, tl,br,Scalar(255), -1);
        }

        rectangle(_destuffed_image, Point(x1, row.rect.br().y + vmargin),
                  Point(x2, row.rect.br().y + _prop.staffspace_height - vmargin),Scalar(255), -1);
    }
}

void row_performer::deleteAllButStuff()
{
    vector< vector<Point> > contours;
    Mat m;

    bitwise_not(_destuffed_image, m);

    findContours(m, contours, RETR_LIST, CV_CHAIN_APPROX_NONE);

    const int DeleteSize = 2 *_prop.staffline_height;

    contours.erase(remove_if(contours.begin(), contours.end(),
        [DeleteSize](vector<Point> &contour){
            Rect bounding = boundingRect(contour);
            return bounding.height <= DeleteSize;
    }), contours.end());

    drawContours(_destuffed_image, contours, -1, Scalar(255), -1);
}

vector<staff_row> row_performer::getPreRows() const
{
    return _pre_rows;
}

vector<Rect> row_performer::getRowRois() const
{
    return _row_rois;
}

