#include "deskewer.h"

void deskewer::deskew_page(Mat &inputMat, const staff_properties prop, Mat source_image)
{
    Rect page_rect(0,0,inputMat.cols, inputMat.rows);
    deskew_rect(inputMat, prop, page_rect, source_image);
}

void deskewer::deskew_rect(Mat &inputMat, const staff_properties prop, Rect rect_to_shear, Mat source_image)
{
    const int StripWidth = 32;

    Mat m = inputMat;
    Range y_range(rect_to_shear.y, rect_to_shear.y + rect_to_shear.height);
    int strips_count = rect_to_shear.width/StripWidth;
    int strip_right = rect_to_shear.width - strips_count*StripWidth;

    const int center_x = rect_to_shear.x + rect_to_shear.width/2;
    Range startRange(center_x, center_x + StripWidth);
    vector<int> mainProj = mat_helper::y_projection(m, startRange, y_range);

    for(int i=strips_count/2+1; i<strips_count; i++){
        int best_shift = find_best_shift(m, mainProj, Range(i*StripWidth, (i+1)*StripWidth), y_range, prop);

        Point move_direction(0, best_shift);
        Rect rect_to_move(i*StripWidth, rect_to_shear.y,
                          rect_to_shear.width - i*StripWidth - strip_right, rect_to_shear.height);

        mat_helper::move_rect(m, move_direction, rect_to_move);
        if(!source_image.empty()){
            mat_helper::move_rect(source_image, move_direction, rect_to_move);
        }
        startRange.end += StripWidth;
        mainProj = mat_helper::y_projection(m, startRange, y_range);
    }

    startRange.start = center_x;
    startRange.end = center_x + StripWidth;
    mainProj = mat_helper::y_projection(m, startRange, y_range); //TODO think about projections

    for(int i=strips_count/2-1; i>=0; i--){
        int best_shift = find_best_shift(m, mainProj, Range(i*StripWidth, (i+1)*StripWidth), y_range, prop);

        Point move_direction(0, best_shift);
        Rect rect_to_move(rect_to_shear.x, rect_to_shear.y, (i+1)*StripWidth, rect_to_shear.height);
        mat_helper::move_rect(m, move_direction, rect_to_move);
        if(!source_image.empty()){
            mat_helper::move_rect(source_image, move_direction, rect_to_move);
        }
        startRange.start -= StripWidth;
        mainProj = mat_helper::y_projection(m, startRange, y_range);
    }
}

int deskewer::find_best_shift(const Mat &mat, vector<int> &main_proj, Range x_range, Range y_range, const staff_properties prop)
{
    vector<int> next_proj = mat_helper::y_projection(mat, x_range, y_range);

    double best_shift_res = 0;
    int bound = (prop.staffline_height/2 + prop.staffspace_height/3);
    int best_shift = -bound;

    for(int jshift=-bound; jshift<bound; jshift++){
        double res = vector_helper::dot_prod_shifted(main_proj, next_proj, jshift);
        if(res > best_shift_res){
            best_shift = jshift;
            best_shift_res = res;
        }
    }

    return best_shift;
}
