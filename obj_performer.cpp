#include "obj_performer.h"

obj_performer::obj_performer(Mat &originalMat, Mat objectMat, vector<staff_row> staffRows,
                             vector<Rect> row_rois, staff_properties staffProp,
                             py_classifier &gameraClassifier) //TODO: make refereses
    : _originalMat(originalMat),
      _objectMat(objectMat),
      _staffRows(staffRows),
      _rowRois(row_rois),
      _staffProp(staffProp),
      _gameraClassifier(gameraClassifier)
{
    _matSize.height = originalMat.rows;
    _matSize.width = originalMat.cols;
}

void obj_performer::clearFromNoise(Mat mat, const vector<Contour> &contours) const
{
    for(uint i=0; i<contours.size(); i++){
        if(boundingRect(contours[i]).width < _staffProp.staffline_height){
            drawContours(mat, contours, i, Scalar(255), -1);
        }
    }
}

vector<classified_object> obj_performer::execClassifier(const Mat &mat, string outFileName, classifier_type type)
{
    string path = mat_helper::save_file(mat, outFileName);

    return _gameraClassifier.execClassifier(path, type);
}

Mat obj_performer::dealWithStems(vector<classified_object> &dealWithObjects)
{
    Mat noteMat = Mat::zeros(_matSize, CV_8UC1);
    for(classified_object object : dealWithObjects){
        Rect rect = object.rect;
        Mat m = Mat::zeros(rect.height, rect.width, CV_8UC1);
        drawContours(m, object.contours, -1, Scalar(255),-1);

        int max_vrun = runlength::find_max_vertical_run(m, true);
        double r = (double) max_vrun / _staffProp.staffspace_height;
        bitwise_not(m,m);
        if(r > 2.7){ //удалили stems //TODO: удаление не с помощью runlength
            runlength::filter_vertical_long_runs(m, 2.5*_staffProp.staffspace_height, false);
            runlength::filter_horizontal_short_runs(m, 2, false);
            runlength::filter_vertical_short_runs(m, 2, false);
        }
        bitwise_not(m,m);

        Mat roiMat = noteMat(rect);
        bitwise_or(m, roiMat, roiMat);
    }
    bitwise_not(noteMat, noteMat);

    return noteMat;
}

void obj_performer::drawObject(Mat &color_mat, Rect objRect, string text, cv_color color)
{
    int fontFace = CV_FONT_HERSHEY_PLAIN;
    double fontScale = 0.8;
    int fontThickness = 1;
    int baseline = 0;
    Size sz = getTextSize(text, fontFace, fontScale, fontThickness, &baseline);
    Point org(objRect.x+objRect.width/2-sz.width/2, objRect.br().y + sz.height);
    Scalar scalarColour = color_helper::getColorScalar(color);

    rectangle(color_mat, objRect, scalarColour);
    putText(color_mat, text, org, fontFace, fontScale, scalarColour, fontThickness);
}

float obj_performer::notePosition(classified_object &note)
{
    Rect note_rect = note.rect; //TODO: change below in code.
    auto max_el = max_element(_rowRois.begin(), _rowRois.end(),
        [note_rect](const Rect &r1, const Rect &r2)
    {
        return (r1 & note_rect).area() < (r2 & note_rect).area();
    });
    int maxPos = max_el - _rowRois.begin();

    staff_row row = _staffRows[maxPos];
    //int additionalspace = 0;
    /*if( note_rect.y < row.rect.y - _staffProp.staffspace_height/2.0 ){//TODO: handle notes out of common rectangle
        additionalspace = -row.rect.height - _staffProp.staffline_height-_staffProp.staffspace_height;
    }
    if( note_rect.y > row.lines.back()+_staffProp.staffspace_height){
        additionalspace = row.rect.height + _staffProp.staffline_height+_staffProp.staffspace_height;
    }*/

    float note_center = note_rect.y + note_rect.height/2.0;
    auto center_min_iterator = min_element(row.lines.begin(), row.lines.end(),
                                           [note_center/*,additionalspace*/](int i1, int i2)
    {
        return fabs(i1-note_center/* + additionalspace*/) < fabs(i2-note_center /*+ additionalspace*/);
    });

    float note_top = note_rect.y;
    auto top_min_iterator = min_element(row.lines.begin(), row.lines.end(),
                                        [note_top/*,additionalspace*/](int i1, int i2)
    {
        return fabs(i1 - note_top/*+ additionalspace*/) < fabs(i2 - note_top/*+ additionalspace*/);
    });

    float result;
    if(fabs(*center_min_iterator - note_center) > fabs(*top_min_iterator - note_top)){
        result = 0.5 + (top_min_iterator - row.lines.begin());
    }else{
        result = center_min_iterator - row.lines.begin();
    }

    return result;
}

Mat obj_performer::prepareMatWithObjects(const Mat &binMat, vector<classified_object> &objects, vector<classified_object> &noteheads)
{
    Mat color_mat = viewer::binToColorMat(binMat);

    size_t found;
    for(size_t i=0; i<objects.size(); i++){
        string objtext = objects[i].id;
        if(objtext.find("_split") != string::npos){
            continue;
        }

        found = objtext.find("dealnext");
        if(found == string::npos){
            string text = "";
            cv_color color;

            if(objtext.find("forte") != string::npos)
                text = "forte", color = cv_color::saddlebrown;
            if(objtext.find("mezzo") != string::npos)
                text = "mezzo", color = cv_color::saddlebrown;
            if(objtext.find("sharp") != string::npos)
                text = "sharp", color = cv_color::seagreen;
            if(objtext.find("flat") != string::npos)
                text = "flat", color = cv_color::seagreen;
            if(objtext.find("time") != string::npos)
                text = "time", color = cv_color::darkgreen;
            if(objtext.find("clef") != string::npos)
                text = objtext, color = cv_color::darkkhaki;

            if(text == "")
                text = objtext, color = cv_color::gold;


            drawObject(color_mat, objects[i].rect, text, color);
        }
    }
    sort(noteheads.begin(), noteheads.end(), [](const classified_object &o1, const classified_object &o2){
        return o1.rect.y < o2.rect.y;
    });
    for(size_t i=0; i<noteheads.size(); i++){
        Rect rect = noteheads[i].rect;
        if(rect.width < _staffProp.staffspace_height/2.5){
            continue;
        }
        string objtext = noteheads[i].id;
        found = objtext.find("note");
        if(found != string::npos){
            float f = notePosition(noteheads[i]);
            string s = to_string(f);
            s.resize(s.size()-5);

            drawObject(color_mat, rect, s, cv_color::darkmagenta);
        }
    }

    for(uint i=0; i<_staffRows.size(); i++){
        rectangle(color_mat, _staffRows[i].rect, color_helper::getColorScalar(cv_color::aqua) );
    }

    for(uint i=0; i<_rowRois.size(); i++){
        rectangle(color_mat, _rowRois[i], color_helper::getColorScalar(cv_color::aquamarine),1);
    }

    return color_mat;
}

void obj_performer::perform()
{
    runlength::filter_vertical_short_runs(_objectMat, 2, false);
    vector<Contour> contours = contour_helper::findExternalContours(_objectMat);
    clearFromNoise(_objectMat, contours);
    contours.erase(remove_if(contours.begin(), contours.end(),
            [this](Contour contour){
                return boundingRect(contour).width < _staffProp.staffline_height;})
            ,contours.end());

    _objMatToReturn = _objectMat.clone();

    vector<classified_object> classifiedObjects = execClassifier(_objectMat, "out_for_classify.png",classifier_type::common);

    vector<classified_object> dealWithObjects(classifiedObjects.size());
    auto it = copy_if(classifiedObjects.begin(),classifiedObjects.end(),dealWithObjects.begin(),
                      [](classified_object &obj){
        uint found = obj.id.find("dealnext");
        return found != std::string::npos;
    });
    dealWithObjects.resize(std::distance(dealWithObjects.begin(), it));

    cout<<"dealWithObjects.size(): "<<dealWithObjects.size()<<"\tclassifiedObjects.size()   "<<classifiedObjects.size()<<endl;

    Mat stemLessMat = dealWithStems(dealWithObjects);

    string outFileName = "out_for_classify_stemless.png";
    mat_helper::save_file(stemLessMat, outFileName);

    //viewer::viewMat("stemLessMat", stemLessMat);

    vector<classified_object> classifiedNotes = execClassifier(stemLessMat, "out_for_classify_notes.png", classifier_type::notehead);
    vector<Rect> rects;
    for(classified_object obj : classifiedNotes){
        uint found = obj.id.find("notehead");
        if(found != std::string::npos){
            rects.push_back(obj.rect);
        }
    }//viewer::viewMat("notes", viewer::viewRects(stemLessMat, rects, Scalar(0,0,255), 2));

    Mat textMat = prepareMatWithObjects(_originalMat, classifiedObjects, classifiedNotes);
    //viewer::viewMat("mat with classes", textMat);
    mat_helper::save_file(textMat, "out_classified.png");
}

