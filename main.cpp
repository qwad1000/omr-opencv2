#include <iostream>
#include <string>

#include <boost/program_options.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/filesystem.hpp>

#include <opencv2/opencv.hpp>
#include <tesseract/baseapi.h>
#include <leptonica/allheaders.h>

#include "my_analyser.h"
#include "helpers/tesseract_cv.h"
#include "helpers/py_classifier.h"


namespace po = boost::program_options;
using namespace cv;
using namespace std;

po::options_description processCommandLineArgs(int argc, char *argv[], po::variables_map &var_map);
int processOneFile(const string &inputFileName, tesseract_cv &tess, py_classifier &gamera_cassifier);

int main(int argc, char** argv)
{
    try {
        po::variables_map args_map;
        po::options_description options_description = processCommandLineArgs(argc, argv, args_map);

        if (args_map.count("help")) {
            cout << "Usage: opencv2 [options]"<<endl;
            cout << options_description << endl;
            return 0;
        }

        string out_folder = args_map["out-folder"].as<string>();
        boost::filesystem::create_directory(out_folder);

        if (args_map.count("input-file")) {
            int tesseract_min_confidence = args_map["text-confidence"].as<int>();
            tesseract_cv tesseract_engine(tesseract_min_confidence);
            string langstr = args_map["ocr-lang"].as<string>();
            tesseract_engine.Init(NULL, langstr.c_str(), tesseract::OEM_DEFAULT);
            tesseract_engine.SetPageSegMode(tesseract::PSM_SPARSE_TEXT);

            py_classifier gameraClassifierHelper(argc, argv);

            auto fileNames = args_map["input-file"].as<vector<string>>();

            for(string &fileName : fileNames){
                boost::filesystem::path path(out_folder + "/" + boost::filesystem::path(fileName).stem().string() + "/");
                boost::filesystem::create_directory(path);
                mat_helper::save_folder = path.string();

                int result = processOneFile(fileName, tesseract_engine, gameraClassifierHelper);
                if(result != 0){
                    cout<<"Some problems was with image "<<fileName<<endl;
                }
            }
        } else {
            cout << "There is noimages to work with\n"
                 << "Type 'yaomr --help' to get help\n";
            return -1;
        }

    } catch(exception &e) {
        cout<<e.what()<<endl;
        return 1;
    }

    return 0;
}

int processOneFile(const string &inputFileName, tesseract_cv &tesseract_engine, py_classifier &gamera_classifier)
{
    Mat image = imread(inputFileName, CV_LOAD_IMAGE_GRAYSCALE);
    if(!image.data) {
        return -1;
    }

    Mat binImage;
    threshold(image, binImage, 200, 255, CV_THRESH_BINARY);

    main_analyser a(binImage, tesseract_engine, gamera_classifier);
    a.perform();

    tesseract_engine.Clear();
    return 0;
}

po::options_description processCommandLineArgs(int argc, char *argv[], po::variables_map &var_map)
{
    po::options_description options_description("Allowed options");
    options_description.add_options()
        ("help", "produce help message")
        ("input-file", po::value<vector<string>>(), "input image file")
        ("ocr-lang", po::value<string>()->default_value("rus+eng"),
         "languages for ocr (currently using tesseract ocr engine). May be as lang1[+lang2[+lang3...]]")
        ("text-confidence", po::value<int>()->default_value(5), "min confidence of text presence in block")
        ("out-folder", po::value<string>()->default_value("out"), "out files folder");

    po::positional_options_description p;
    p.add("input-file", -1);

    po::store(po::command_line_parser(argc, argv).options(options_description).positional(p).run(), var_map);
    po::notify(var_map);

    return options_description;
}
