#ifndef MY_STRUCTS
#define MY_STRUCTS

#include <opencv2/opencv.hpp>
#include <vector>

using namespace std;
using namespace cv;

typedef vector<Point> Contour;

/**
 * @struct staff_properties
 * @brief структура, що інкапсулює основні характеристики нотного стану (висоту лінії нотного стану і проміжок між ними)
 */
struct staff_properties {
    int staffline_height;   /**< висота ліній нотного стану */
    int staffspace_height;  /**< проміжок між лініями нотного стану одного нотного рядка */
    staff_properties(const int staffline_height, const int staffspace_height)
        : staffline_height(staffline_height),
          staffspace_height(staffspace_height)
    {}
};

/**
 * @struct staff_row
 * @brief структура що включає інформацію про рядок нотного стану
 */
struct staff_row {
    vector<int> lines;  /**< позиції ліній нотного стану*/
    Rect rect;          /**< прямокутник рядку нотного стану*/
};

/**
 * @struct text_rect
 * @brief структура, що включає текстову інформацію і її позицію
 */
struct text_rect {
    string text;        /**< текст */
    Rect rect;          /**< область тексту на сторінці */

    text_rect(const string &a_text, const Rect &a_rect)
        : text(a_text),
          rect(a_rect)
    {}
};
/**
 * @struct classified_object
 * @brief структура, що інкапсулює інформацію про класифікований об’єкт.
 */
struct classified_object {
    string id;                  /**< назва класу об’єкта */
    Rect rect;                  /**< прямокутна область об’єкта */
    vector<Contour> contours;   /**< контури класифікованого об’єкта */

    classified_object()
    {}
    classified_object(const string &id_name, const Rect &rect, const vector<Contour> &a_contours)
        : id(id_name),
          rect(rect),
          contours(a_contours)
    {}
};

#endif // MY_STRUCTS

