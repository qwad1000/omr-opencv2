from docutils.nodes import classifier

__author__ = 'qwad1000'

import sys
from gamera.core import *
from gamera import knn
from gamera.plugins import fourier_features
from gamera.plugins import runlength

def my_application(image_path, classifier):
    print 'my_app started'

    init_gamera()
    print 'gamera initialisation finished'
    image = load_image(image_path)
    print image_path, ' is loaded'

    global oneBitImage  # global for future exec with gamera.gui
    oneBitImage = image.to_onebit()

    ccs = oneBitImage.cc_analysis()
    classifier.group_and_update_list_automatic(ccs)
    print 'From python: classifying is over'

    splitccs = []
    for elem in ccs:
        flag = False
        for id_name in elem.id_name:
            if "_split" in id_name[1]:
                flag = True
        if flag:
            splitccs.append(elem)
            elem.id_name = []
            elem.classification_state = UNCLASSIFIED

    added, removed = classifier.classify_list_automatic(splitccs, 2)

    result = ccs + added
    for g in removed:
        if g in result:
            result.remove(g)

    return result


def load_classifier(knn_data_path):
    feature_list = ['aspect_ratio', 'diagonal_projection', 'fourier_broken', 'moments', 'nholes', 'nholes_extended',
                    'skeleton_features', 'volume', 'volume16regions', 'volume64regions', 'zernike_moments']
    perform_splits = True
    classifier = knn.kNNNonInteractive(knn_data_path, feature_list, perform_splits)
    print 'train data ', knn_data_path, ' is loaded'
    return classifier


def main(im_file_name, knn_data_path):
    knn_classifier = load_classifier(knn_data_path)
    my_application(im_file_name, knn_classifier)
    # from gamera.gui import gui
    # gui.run(my_application(im_file_name, knn_classifier))


if __name__ == "__main__":
    execName = sys.argv[0]
    if 'my_py_test.py' in execName:
        imFileName = "out_for_classify.png"
        knnDataFileName = "data/common_classifier_glyphs.xml"
        main(imFileName, knnDataFileName)
    else:
        print 'python from c++ started'

