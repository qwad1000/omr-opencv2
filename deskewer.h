#ifndef DESKEWER_H
#define DESKEWER_H

#include <vector>
#include <opencv2/opencv.hpp>
#include <limits>
#include <cmath>

#include "my_structs.h"
#include "helpers/viewer.h"
#include "helpers/mat_helper.h"
#include "helpers/vector_helper.h"

using namespace std;
using namespace cv;

/**
 * @brief deskewer цілком статичний клас для вирівнювання сторінки методом вертикальних стрічок
 */
class deskewer
{
public:
    /**
     * @brief deskew_page       вирівнює все зображення по горизонталі
     * @param inputMat
     * @param prop
     * @param source_image
     */
    static void deskew_page(Mat &inputMat, const staff_properties prop, Mat source_image = Mat());
    /**
     * @brief deskew_rect       вирівнює по горизонталі зазначений прямокутник
     * @param inputMat          матриця для вирівнювання
     * @param prop              характеристики нотного стану
     * @param rect_to_shear     прямокутник, на якому проводити вирівнювання
     * @param source_image      якщо зазначений, виконується вирівнювання
     */
    static void deskew_rect(Mat &inputMat, const staff_properties prop, Rect rect_to_shear, Mat source_image = Mat());
private:
    static int find_best_shift(const Mat &mat, vector<int> &main_proj, Range x_range, Range y_range, const staff_properties prop);
};
#endif // DESKEWER_H
