#ifndef MASK_H
#define MASK_H

#include <vector>
#include "my_structs.h"

using namespace std;

class mask
{
    staff_properties _prop;
public:
    mask(const staff_properties prop)
        :_prop(prop)   {}

    int staff_line(const vector<int> &v, int pos) const;
};

#endif // MASK_H



