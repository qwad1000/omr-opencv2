TARGET = yaomr

TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

CONFIG += link_pkgconfig
PKGCONFIG += opencv

QMAKE_CXXFLAGS += -std=c++11

SOURCES += main.cpp \
    deskewer.cpp \
    my_analyser.cpp \
    mask.cpp \
    row_performer.cpp \
    helpers/viewer.cpp \
    helpers/vector_helper.cpp \
    helpers/mat_helper.cpp \
    helpers/tesseract_cv.cpp \
    helpers/contour_helper.cpp \
    ocr_performer.cpp \
    obj_performer.cpp \
    helpers/py_classifier.cpp \
    helpers/runlength.cpp

include(deployment.pri)
qtcAddDeployment()

HEADERS += \
    deskewer.h \
    my_analyser.h \
    my_structs.h \
    mask.h \
    row_performer.h \
    helpers/viewer.h \
    helpers/vector_helper.h \
    helpers/mat_helper.h \
    helpers/tesseract_cv.h \
    helpers/contour_helper.h \
    ocr_performer.h \
    obj_performer.h \
    helpers/py_classifier.h \
    helpers/runlength.h \
    helpers/color_helper.h

LIBS += -llept\
        -ltesseract\
        #boost lib
        -lboost_system\
        -lboost_filesystem\
        -lboost_program_options\
        -lboost_python\
        #python c/api
        -lpython2.7

INCLUDEPATH += /usr/include/python2.7/#TODO: make this in proper way.

#копирование данных для запуска питона
copydata.commands = $(COPY_DIR) $$PWD/my_python $$OUT_PWD
first.depends = $(first) copydata
export(first.depends)
export(copydata.commands)
QMAKE_EXTRA_TARGETS += first copydata
