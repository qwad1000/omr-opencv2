#include "mask.h"

int mask::staff_line(const vector<int> &v, int pos) const
{
    int res = 0;
    int start = pos;// - _prop.staffline_height/2;
    int end = pos + _prop.staffline_height;

    for(int i=start; i<end; i++){
        res += v[i];
    }

    return res;
}
