#ifndef OBJ_PERFORMER_H
#define OBJ_PERFORMER_H

#include <opencv2/opencv.hpp>
#include <utility>

#include "my_structs.h"
#include "helpers/viewer.h"
#include "helpers/contour_helper.h"
#include "helpers/vector_helper.h"
#include "helpers/mat_helper.h"
#include "helpers/py_classifier.h"
#include "helpers/color_helper.h"

using namespace std;
using namespace cv;

/**
 * @class obj_performer
 * @brief obj_performer - клас для пошуку та класифікації об’єктів на зображенні
 */
class obj_performer
{
    Mat _originalMat;
    Mat _objectMat;
    vector<staff_row> _staffRows;
    vector<Rect> _rowRois;
    staff_properties _staffProp;

    Mat _objMatToReturn; //TODO: delete this field. use _objectMat instead;

    Size _matSize;

    py_classifier &_gameraClassifier;
public:
    /**
     * @brief obj_performer єдиний конструктор для класу
     * @param originalMat   оригінальне зображення
     * @param objectMat     зображення після попередньої обробки - з об’єктами для пошуку і класифікації
     * @param staffRows     колекція інформації про рядки нотного стану на зображенні
     * @param row_rois      колекція областей рядків нотного стану
     * @param staffProp     основні характеристики нотного стану
     * @param gameraClassifier  посилання на об’єкт-класифікатор
     */
    obj_performer(Mat &originalMat, Mat objectMat, vector<staff_row> staffRows,
                  vector<Rect> row_rois, staff_properties staffProp,
                  py_classifier &gameraClassifier);

    /**
     * @brief perform головна функція класу
     */
    void perform();
    Mat getClearObjMat() const{
        return _objMatToReturn;
    }

private:
    /**
     * @brief clearFromNoise видалення об’єктів з матриці за малою шириною
     * @param mat            зображення для видалення
     * @param contours       колекція контурів об’єктів
     */
    void clearFromNoise(Mat mat, const vector<Contour> &contours) const;

    /**
     * @brief execClassifier виконує класифікацію об’єктів на зображенні
     * @param mat            зображення для класифікації
     * @param outFileName    назва файлу для збереження зображення
     * @param type           тип класифікатора
     * @return               колекцію класифікованих об’єктів
     */
    vector<classified_object> execClassifier(const Mat &mat, string outFileName, classifier_type type);

    /**
     * @brief dealWithStems видаляє штилі на об’єктах, що були класифіковані як ноти
     * @param dealWithObjects колекція об’єктів, класифікованих як ноти
     * @return  зображення тільки з голівками нот, прапорцями та ребрами довжин
     */
    Mat dealWithStems(vector<classified_object> &dealWithObjects);

    /**
     * @brief drawObject    рисує класифікаційну оболонку з текстом для об’єкту
     * @param color_mat     кольорове зображення для рисування
     * @param objRect       оболонка для рисування
     * @param text          підпис об’єкту
     * @param color         колір рисування
     */
    void drawObject(Mat &color_mat, Rect objRect, string text, cv_color color);
    /**
     * @brief notePosition  знаходить позицію ноти на нотном стані
     * @param note          власне, нота
     * @return позицію ноти на нотному стані - 0, якщо перша лінія, 0.5, якщо між першою та другою тощо.
     */
    float notePosition(classified_object &note);
    /**
     * @brief prepareMatWithObjects вивід інформації про класифіковані об’єкти на зображення
     * @param binMat    матриця для виводу
     * @param objects   класифіковані об’єкти різних типів (окрім нот та довжин)
     * @param noteheads класифіковані об’єкти довжин та нотних голівок
     * @return          кольорове зображення з інформацією про класифіковані об’єкти на зображенні
     */
    Mat prepareMatWithObjects(const Mat &binMat, vector<classified_object> &objects, vector<classified_object> &noteheads);
};

#endif // OBJ_PERFORMER_H
