#include "my_analyser.h"

main_analyser::main_analyser(const Mat &source_image_mat, tesseract_cv &tess, py_classifier &gameraClassifier)
    :_source_image(source_image_mat),
      _tess(tess),
      _gameraClassifier(gameraClassifier)
{
    cerr<<"source image depth: "<<source_image_mat.depth()<<endl;
}

main_analyser::~main_analyser()
{

}

void main_analyser::clearImageBounds(Mat &image, int boundWidth)
{
    Mat mask = Mat::zeros(image.rows, image.cols, CV_8UC1);
    Mat white;
    bitwise_not(mask, white);

    Point ul(boundWidth, boundWidth);
    Point br(image.cols - boundWidth, image.rows - boundWidth);
    rectangle(mask, ul, br, Scalar(255), -1);
    bitwise_not(mask, mask);

    bitwise_or(image, white, image, mask);
}

staff_properties main_analyser::calculate_staff_properties(const Mat &image)
{
    vector< vector<int> > black( image.cols );
    vector< vector<int> > white( image.cols );

    for(int i=0; i<image.cols; i++){
        bool is_white = (image.at<uchar>(0,i) > 0);
        int count = 0;

        for(int j=0; j<image.rows; j++){
            bool curr_is_white = image.at<uchar>(j,i) > 0;
            if(curr_is_white == is_white){
                count++;
            }else{
                (is_white ? white : black)[i].push_back(count);
                count = 1;
                is_white = curr_is_white;
            }

        }
        vector<int> &col = (is_white ? white[i] : black[i]);
        if(count>0 && col.size() == 0){
            col.push_back(count);
        }
    }

    map<int, int> black_map;
    map<int, int> white_map;

    for(int i=0; i<image.cols; i++){
        for(uint j=0; j<black[i].size(); j++){
            int value = black[i][j];
            if(black_map.count(value) == 0){
                black_map[value] = 1;
            }else{
                black_map[value]++;
            }
        }
        for(uint j=0; j<white[i].size(); j++){
            int value = white[i][j];
            if(white_map.count(value) == 0){
                white_map[value] = 1;
            }else{
                white_map[value]++;
            }
        }
    }

    black_map.erase(0);
    white_map.erase(0);

    int max_index = -1;
    int max = 0;
    for(auto i=white_map.begin(); i!=white_map.end(); i++){
        if(max < i->second){
            max = i->second;
            max_index = i->first;
        }
    }

    int staffspace_height = max_index;

    max = 0;
    for(auto i=black_map.begin(); i!=black_map.end(); i++){
        if(max < i->second){
            max = i->second;
            max_index = i->first;
        }
    }
    int staffline_height = max_index;

    return staff_properties(staffline_height, staffspace_height);
}

void main_analyser::deleteStuff(const Mat& originalMat, const int deleteHeight){ //TODO: refactor with runlength class
    Mat m = originalMat;

    for(int i=0; i<m.cols; i++){
        int black_count = 0;
        bool cont = false;
        if(m.at<uchar>(0,i) == 0){
            cont = true;
            black_count = 1;
        }
        for(int j=1; j<m.rows; j++){
            int pixel = m.at<uchar>(j,i);
            if(pixel == 0) {
                cont = true;
                black_count++;
            }else{
                if(cont){
                    if(black_count >= deleteHeight){
                        for(int z=1; z<black_count+1; z++ ){
                            m.at<uchar>(j-z,i) = 255;
                        }
                    }
                }
                cont = false;
                black_count = 0;
            }
        }
    }
}

vector<Contour> main_analyser::find_tall_objects(Mat &destuffedskewed_mat, staff_properties prop)
{
    vector <Contour> contours;
    Mat m;
    bitwise_not(destuffedskewed_mat, m);

    findContours(m, contours, RETR_LIST, CV_CHAIN_APPROX_SIMPLE);

    const int DeleteSize = prop.staffline_height + prop.staffspace_height;
    cout<<"erased from im if height >= "<<DeleteSize<<endl;

    contours.erase(remove_if(contours.begin(), contours.end(),
                             [DeleteSize](Contour &contour)
    {
        Rect bounding = boundingRect(contour);
        return bounding.height < DeleteSize; //todo: not constant
    }), contours.end());

    drawContours(destuffedskewed_mat, contours, -1, Scalar(255), -1);

    return contours;
}

void main_analyser::perform()
{
    clearImageBounds(_source_image);
    staff_properties staff_prop = calculate_staff_properties(_source_image);

    Mat destuffedMat = _source_image.clone();
    deleteStuff(destuffedMat, 2*staff_prop.staffline_height);
    //deleteSmallStuff(destuffedMat, staff_prop);

    Mat deskewed_mat = destuffedMat.clone();
    deskewer::deskew_page(deskewed_mat, staff_prop, _source_image);

    vector<Contour> tall_components =
            find_tall_objects(deskewed_mat, staff_prop);

    // поидее должно более точно показывать значения
    staff_properties staff_prop2 = calculate_staff_properties(deskewed_mat);
    cout<<"precise staff properties: "<<endl
        <<"\tstaffline height: "<<staff_prop2.staffline_height<<endl
        <<"\tstaffspace height: "<<staff_prop2.staffspace_height<<endl;

//    viewer::viewMat("view", deskewed_mat);

    row_performer rowPerformer(_source_image, deskewed_mat, staff_prop2);
    rowPerformer.perform();

//    viewer::viewMat("view1", deskewed_mat);

    ocr_performer ocrPerformer(_source_image, _tess, rowPerformer.getPreRows(), tall_components);
    vector<Rect> ocr_rects = ocrPerformer.perform();

    Mat ocr_mask = mat_helper::create_mask(ocr_rects, Size(deskewed_mat.cols, deskewed_mat.rows), deskewed_mat.type());
    bitwise_not(ocr_mask, ocr_mask);

    Mat cleared_mat;
    bitwise_xor(deskewed_mat,_source_image, cleared_mat, ocr_mask);
    bitwise_not(cleared_mat,cleared_mat);

    //viewer::viewMat("outcoming image", cleared_mat, 10);

    drawContours(cleared_mat, tall_components, -1, Scalar(255),-1);

    obj_performer objPerformer(_source_image, cleared_mat, rowPerformer.getPreRows(),
                               rowPerformer.getRowRois(), staff_prop2,
                               _gameraClassifier);
    objPerformer.perform();

    _objMat = objPerformer.getClearObjMat();
}


//TODO : пересмотреть удаление объекта
//A connected component analysis is then performed on the filtered image and
//any component whose width is less than staffspace_height is removed (Figure
