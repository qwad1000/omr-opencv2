#include "ocr_performer.h"

ocr_performer::ocr_performer(Mat originalMat, tesseract_cv &tess, const vector<staff_row> &rows, vector<vector<Point> > &tall_contours)
    : _original_mat(originalMat),
      _tess(tess),
      _staff_rows(rows),
      _tall_contours(tall_contours)
{
    _mat = originalMat.clone();
}

vector<Rect> ocr_performer::perform()
{
    drawContours(_mat, _tall_contours, -1, Scalar(255), -1); // очищаем от контуров высоких объектов
    clearFromRows();
    _tess.SetImage(_mat);
    auto rects = findTextRects();

    _text_rects = findText(rects, _tess.getMinConfidence());
    rects.clear();
    for(auto tr : _text_rects){
        rects.push_back(tr.rect);
    }
    //TODO: make rects more precise
    return rects;
}

vector<text_rect> ocr_performer::getTextRects() const
{
    return _text_rects;
}

void ocr_performer::clearFromRows()
{
    Mat m;
    bitwise_not(_mat, m);

    vector< vector<Point> > contours;
    findContours(m, contours, RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);

    for(auto row : _staff_rows){
        Rect rowRect = row.rect;

        int max = 0;
        int max_pos = 0;
        for(uint i=0; i<contours.size(); i++){
            Rect contourRect = boundingRect(contours[i]);

            int currArea = (contourRect & rowRect).area();
            if(max < currArea){
                max = currArea;
                max_pos = i;
            }
        }
        drawContours(_mat, contours, max_pos, Scalar(255), -1);
    }
}

vector<Rect> ocr_performer::findTextRects()
{
    auto rects = _tess.GetRegions();

    return rects;
}

vector<text_rect> ocr_performer::findText(vector<Rect> &rects, const int min_confidence)
{
    vector<text_rect> text_rects;
    vector<Rect> debug_rects;

    for(Rect &rect : rects){
        _tess.SetRectangle(rect);
        string str = _tess.GetUTF8Text();
        int confidence = _tess.MeanTextConf();

        if(confidence > min_confidence){
            text_rects.push_back(text_rect(str,rect));
            debug_rects.push_back(rect);
        }
    }


    cout<<"text recognized with "<<min_confidence<<"% in "
        <<text_rects.size()<<"rectangles from "<<rects.size()<<"rects"<<endl;

//    viewer::viewMat("ocr::rects", viewer::viewRects(_mat, debug_rects, Scalar(0,255,255),4) );
    //TODO: Добавить вызов SetSourceResolution и как-то достать размеры шрифтов. Это возможно - гарантия 99%

    return text_rects;
}


