#ifndef ROW_PERFORMER_H
#define ROW_PERFORMER_H

#include <vector>
#include <opencv2/opencv.hpp>

#include "deskewer.h"
#include "my_structs.h"
#include "mask.h"

#include "helpers/vector_helper.h"
#include "helpers/viewer.h"
#include "helpers/runlength.h"

using namespace std;
using namespace cv;

/**
 * @brief row_performer клас для роботи з рядками нот на сторінці
 */
class row_performer
{
    Mat _source_image;
    Mat _destuffed_image;
    staff_properties _prop;
    vector<staff_row> _pre_rows;
    vector<Rect> _row_rois;

public:
    /**
     * @brief row_performer     єдиний конструктор
     * @param source_image      оригінальне зображення в бітовому форматі
     * @param destufed_image    зображення без нот
     * @param prop              основні характеристики нотного стану
     */
    row_performer(Mat &source_image, Mat &destufed_image, const staff_properties &prop);

    /**
     * @brief perform головна функція класу
     */
    void perform();

    /**
     * @brief find_rows пошук
     * @param deskewed_mat
     * @param staff_prop
     * @return
     */
    vector<staff_row> find_rows(const Mat &deskewed_mat, const staff_properties &staff_prop);

    /**
     * @brief findRowROIs виконує пошук рядків нотного стану
     * @return колекцію прямокутних областей рядків нотного стану
     */
    vector<Rect> findRowROIs();

    /**
     * @brief deleteBetweenStaffLines   видаляє об’єкти між лініями нотного стану
     * @param vmargin                   запобіжна відстань
     */
    void deleteBetweenStaffLines(const int vmargin);
//    void deleteBetweenStaffs(const int vmargin);

    /**
     * @brief deleteAllButStuff видаляє елементи висоти більшої, ніж 2 висоти лінії нотного стану
     */
    void deleteAllButStuff();

    vector<staff_row> getPreRows() const;
    vector<Rect> getRowRois() const;
private:
    vector<staff_row> find_rows_y(const Mat &destuffed, const staff_properties &staff_prop);
    void find_rows_x(vector<staff_row> &rows_y) const;
};

#endif // ROW_PERFORMER_H
