#ifndef OCR_PERFORMER_H
#define OCR_PERFORMER_H

#include <opencv2/opencv.hpp>
#include <leptonica/allheaders.h>
#include <tesseract/baseapi.h>

#include "helpers/tesseract_cv.h"
#include "my_structs.h"
#include "helpers/viewer.h"
#include "helpers/mat_helper.h"

using namespace cv;
using namespace std;

/**
 * @brief ocr_performer - клас для роботи з текстовим шаром на вхідному зображенні
 */
class ocr_performer
{
    Mat _original_mat;
    Mat _mat;
    tesseract_cv &_tess;
    vector<staff_row> _staff_rows;
    vector<vector<Point>> _tall_contours;

    vector<text_rect> _text_rects;
    int _min_confidence;

public:
    /**
     * @brief ocr_performer єдиний конструктор класу.
     * @param originalMat   бінарне зображення, на якому потрібно знайти текстову інформацію
     * @param tess          посилання на об’єкт ocr-аналізатора tesseract
     * @param rows          вектор з даними про знайдені рядки нотного стану за вхідному зображенні
     * @param tall_contours контури на вхідному зображенні
     */
    ocr_performer(Mat originalMat, tesseract_cv &tess, const vector<staff_row> &rows,
                  vector<vector<Point>> &tall_contours);

    /**
     * @brief perform   основна функція роботи класу - розпізнавання блоків
     * @return          колекцію паралелограмів, в яких було знайдено текст
     */
    vector<Rect> perform();
    /**
     * @return колекцію паралелограмів, в яких було знайдено текст
     */
    vector<text_rect> getTextRects() const;
private:
    /**
     * @brief clearFromRows видалення рядків нотного стану з вхідного зображення
     */
    void clearFromRows();

    /**
     * @return знайдену колекцію паралелограмів з текстом
     */
    vector<Rect> findTextRects();

    /**
     * @brief findText  знаходить, власне, текст на вхідному зображенні в попередньо знайдених областях
     * @param rects     області, де потрібно шукати текст
     * @param min_confidence мінімальна ймовірність, за якої знайдена інформація в області вважається текстом
     * @return колекцію пар текст - позиція тексту
     */
    vector<text_rect> findText(vector<Rect> &rects, const int min_confidence = 20);

};

#endif // OCR_PERFORMER_H
