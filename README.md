Це проект з відкритим вихідним кодом - архітектура та два модулі системи автоматичного розпізнавання нотного запису ([OMR](http://uk.wikipedia.org/w/index.php?title=Music_OCR)) створений під час виконання дипломної роботи. 

### Як його запустити? ###
* Збирання проекту проводиться з qmake
* Залежності:
    - c++ boost
    - [tesseract-ocr](https://code.google.com/p/tesseract-ocr/)
    - [OpenCV](http://opencv.org/)
    - python 2.7
    - [Gamera](http://gamera.informatik.hsnr.de/)

* Процес збирання
    - встановити python2.7, OpenCV, boost, Gamera, tesseract-ocr та leptonica.
    - запустити qmake
    - запустити make
    - (опціонально для Linux) запустити make install.