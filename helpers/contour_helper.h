#ifndef CONTOUR_HELPER_H
#define CONTOUR_HELPER_H

#include <vector>
#include <opencv2/opencv.hpp>

#include "my_structs.h"

using namespace cv;
using namespace std;

class contour_helper
{
public:
    /**
     * @brief contour_helper::getXRange возвращает диапазон по оси X, в котором лежит контур
     * @param contour
     * @return
     */
    static Range getXRange(vector<Point> contour);
    /**
     * @brief contour_helper::getYRange возвращает диапазон по оси X, в котором лежит контур
     * @param contour
     * @return
     */
    static Range getYRange(vector<Point> contour);
    static vector<Contour> findExternalContours(const Mat &image);
    static vector<Rect> boundingRectangles(const vector<Contour> &contours);

private:
    /**
     * @brief contour_helper::inRange
     * @param r
     * @param value
     * @return 0, если точка находится внутри диапазона. -1, если левее, 1, если правее
     */
    static int inRange(const Range &r, const int value);
};

#endif // CONTOUR_HELPER_H
