#ifndef VECTOR_HELPER_H
#define VECTOR_HELPER_H

#include <vector>
#include <opencv2/opencv.hpp>
#include <cmath>

using namespace std;
using namespace cv;

class vector_helper
{
public:
    static vector<int> left_derivate(const vector<int> &v);
    static vector<double> center_derivate(const vector<int> &v);
    static vector<int> right_derivate(const vector<int> &v);
    static double avg(const vector<int> &v);
    static double avg(const vector<int> &v, const Range range);

    static double expected(const vector<int> &v, Range range = Range(-1,-1));
    static double expected(const vector<int> &v, function<double(const double)> f, Range range = Range(-1,-1));

    static vector<Range> getRanges(const vector<int> &v, const int leak_white_range = 0);

    static double correl(const vector<int> &v1, const vector<int> &v2, int shift = 0);
    /**
     * @brief deskewer::dot_prod_shifted работает только в предположении, что по краям все белое
     * @param a
     * @param b
     * @param shift сдвиг b относительно a. Если shift < 0 - b сдвинут выше.
     * @return
     */
    static double dot_prod_shifted(const vector<int> &a, const vector<int> &b, int shift);
};

#endif // VECTOR_HELPER_H
