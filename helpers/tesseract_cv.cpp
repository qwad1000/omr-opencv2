#include "tesseract_cv.h"

tesseract_cv::tesseract_cv(const int min_confidence)
    : min_confidence(min_confidence)
{
}

void tesseract_cv::SetImage(const Mat &mat)
{
    TessBaseAPI::SetImage((uchar*)mat.data, mat.cols, mat.rows, 1, mat.cols);
}

void tesseract_cv::SetRectangle(const Rect &rect)
{
    TessBaseAPI::SetRectangle(rect.x, rect.y, rect.width, rect.height);
}

vector<Rect> tesseract_cv::GetRegions()
{
    Boxa *boxa = TessBaseAPI::GetRegions(NULL);

    vector<Rect> regions(boxa->n);
    for(int i=0; i<boxa->n; i++){
        Box *box = boxa->box[i];
        regions[i] = Rect(box->x, box->y, box->w, box->h);
    }

    return regions;
}

string tesseract_cv::GetUTF8Text()
{
    char *c_str = TessBaseAPI::GetUTF8Text();
    string s(c_str);

    delete [] c_str;

    return s;
}


int tesseract_cv::getMinConfidence() const
{
    return min_confidence;
}

void tesseract_cv::setMin_confidence(int confidence)
{
    min_confidence = confidence;
}
