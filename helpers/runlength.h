#ifndef RUNLENGTH_H
#define RUNLENGTH_H

#include <vector>
#include <opencv2/opencv.hpp>
#include <boost/tokenizer.hpp>

#include <algorithm>
#include <functional>

#include "helpers/viewer.h"

using namespace cv;
using namespace std;

/**
 * @brief runlength цілком статичний клас для роботи з кодуванням run-length типу
 */
class runlength
{
public:
    /**
     * @brief to_rle    переводить зображення у rle вектор.
     * @param mat       вхідне зображення
     * @return          вектор кодування довжин, що починається завжди з білого блоку.
     */
    static vector<int> to_rle(const Mat &mat);
    static Mat from_rle(const Size mat_size, const vector<int> &rle);
    /**
     * @brief from_rle  розкодування rle в двійкове зображення
     * @param mat_size  розмір зображення - Size(cols, rows)
     * @param rle       строка з цілими числами через роздільник
     * @return
     */
    static Mat from_rle(const Size mat_size, const string &rle);

    static void filter_vertical_run(Mat &m, int length, int col, const function<bool(int,int)> &functor, bool color);
    /**
     * @brief filter_vertical_runs  видаляє всі вертиальні блоки зображення, що  мають специфічну довжину
     * @param m         зображення
     * @param length    довжина блоку
     * @param functor   функція порівняння довжини блоку (std::less, std::greater)
     * @param color     колір блоку для видалення
     */
    static void filter_vertical_runs(Mat &m, int length, const function<bool(int, int)> &functor, bool color);
    static void filter_vertical_short_runs(Mat &m, int length, bool whitecolor);
    static void filter_vertical_long_runs(Mat &m, int length, bool whitecolor);
    static int find_max_vertical_run(Mat &m, int column, bool color);
    static int find_max_vertical_run(Mat &m, bool color);

    static void filter_horizontal_run(Mat &m, int length, int row, const function<bool(int,int)> &functor, bool color);
    /**
     * @brief filter_horizontal_runs  видаляє всі горизонтальні блоки зображення, що  мають специфічну довжину
     * @param m         зображення
     * @param length    довжина блоку
     * @param functor   функція порівняння довжини блоку (std::less, std::greater)
     * @param color     колір блоку для видалення
     */
    static void filter_horizontal_runs(Mat &m, int length, const function<bool(int, int)> &functor, bool color);
    static void filter_horizontal_short_runs(Mat &m, int length, bool whitecolor);
    static void filter_horizontal_long_runs(Mat &m, int length, bool whitecolor);

private:
    static inline int run_end_vertical(const Mat &m, int rstart, int rend, int col);
    static inline void fill_verctical_run(Mat &m, int rstart, int rend, int col, bool whitecolor);

    static inline int run_end_horizontal(const Mat &m, int cstart, int cend, int row);
    static inline void fill_horizontal_run(Mat &m, int cstart, int cend, int row, bool whitecolor);

    static inline MatConstIterator_<uchar> run_end(MatConstIterator_<uchar> i, MatConstIterator_<uchar> end, bool color);
};

#endif // RUNLENGTH_H
