#ifndef PY_CLASSIFIER_H
#define PY_CLASSIFIER_H

#include <iostream>
#include <list>
#include "python2.7/Python.h"

#include <boost/python.hpp>
#include <boost/python/converter/from_python.hpp>

#include <opencv2/opencv.hpp>

#include "my_structs.h"
#include "helpers/runlength.h"

using namespace std;

namespace py = boost::python;

/**
 * @brief classifier_type перелік можливих класифікаторів
 */
enum class classifier_type {
    common,     /**< початковий класифікатор об’єктів */
    notehead    /**< класифікатор об’єктів нот і їх довжин */
};
/**
 * @brief py_classifier - клас для роботи з бібліотекою gamera на python
 */
class py_classifier
{
    const string PythonScriptPath = "my_python/my_py_test.py";
    const string CommonClassifierDataPath = "my_python/data/common_classifier_glyphs.xml";
    const string NoteheadClassifierDataPath = "my_python/data/notehead_classifier_glyphs.xml";

    py::object _mainModule;
    py::object _mainNameSpace;
    py::object _startClassifyFunction;

    py::object _getRunlengthFunction;

    py::object _commonClassifierObject;
    py::object _noteheadClassifierObject;
public:
    /**
     * @brief py_classifier - єдиний конструктор. Ініціалізує інтерпретатор python
     * @param argc,argv  параметри командної строки
     */
    py_classifier(int argc, char **argv);
    ~py_classifier();

    /**
     * @brief execClassifier    виконує класифікацію об’єктів з зовнішнього зображення
     * @param image_path        адреса зовнішнього зображення
     * @param classifiertype    тип класифікатора @see classifier_type
     * @return
     */
    vector<classified_object> execClassifier(string image_path = "my_python/test_im.png",
                                             classifier_type classifiertype = classifier_type::common);

private:
    static vector<Contour> createContoursFromRunlength(string &rle, Size sz);
    static string parse_python_exception();
};

#endif // PY_CLASSIFIER_H
