#include "py_classifier.h"

py_classifier::py_classifier(int argc, char **argv)
{
    Py_Initialize();
    PySys_SetArgv(argc, argv);

    _mainModule = py::import("__main__");
    _mainNameSpace = _mainModule.attr("__dict__");

    try {
        py::exec_file(PythonScriptPath.c_str(), _mainNameSpace);

        _startClassifyFunction = _mainModule.attr("my_application");
        py::object load_classyfier_function = _mainModule.attr("load_classifier");

        _commonClassifierObject = load_classyfier_function(CommonClassifierDataPath);
        _noteheadClassifierObject = load_classyfier_function(NoteheadClassifierDataPath);
    } catch(boost::python::error_already_set const &) {
        std::string perror_str = parse_python_exception();
        std::cout << "Error in Python: " << perror_str << std::endl;
    }
}

py_classifier::~py_classifier()
{
    //Py_Finalize(); currently unavailable
}

vector<classified_object> py_classifier::execClassifier(string image_path, classifier_type classifiertype)
{
    std::vector<classified_object> classifiedObjects;
    py::object &classifier = (classifiertype == classifier_type::common)
                ? _commonClassifierObject : _noteheadClassifierObject;

    try {
        py::object ccs = _startClassifyFunction(image_path, classifier);

        py::list glyphs_list = py::extract<py::list>(ccs);
        const uint glyphs_len = py::len(glyphs_list);

        for(uint i=0; i<glyphs_len; i++){
            py::object element_obj = py::extract<py::object>(glyphs_list[i]);

            ///deal with rectangle
            py::object dim = element_obj.attr("dim");
            int ncols = py::extract<int>(dim.attr("ncols"));
            int nrows = py::extract<int>(dim.attr("nrows"));

            py::object ul = element_obj.attr("ul");
            int x = py::extract<int>(ul.attr("x"));
            int y = py::extract<int>(ul.attr("y"));

            cv::Rect rect(x,y,ncols, nrows);

            //deal with class names id_name
            py::list id_names = py::extract<py::list>(element_obj.attr("id_name"));
            uint id_name_len = py::len(id_names);
            string future_class_name;
            if(id_name_len > 0){
                py::tuple id_name = py::extract<py::tuple>(id_names[0]);
                //float probability = py::extract<float>(id_name[0]);
                future_class_name = py::extract<std::string>(id_name[1]);
            }

            string runlength_str = py::extract<string>(element_obj.attr("to_rle")());//TODO: test!!!!
            vector<Contour> contours = createContoursFromRunlength(runlength_str, Size(rect.width, rect.height));

            classifiedObjects.push_back(classified_object(future_class_name, rect, contours));
        }
    } catch(boost::python::error_already_set const &) {
        std::string perror_str = parse_python_exception();
        std::cout << "Error in Python: " << perror_str << std::endl;
    }
    return classifiedObjects;
}

vector<Contour> py_classifier::createContoursFromRunlength(string &rle, Size sz)
{
    Mat mat = runlength::from_rle(sz, rle);
    bitwise_not(mat, mat);

    vector<Contour> contours;
    findContours(mat, contours, RETR_LIST, CV_CHAIN_APPROX_SIMPLE);

    return contours;
}


string py_classifier::parse_python_exception()
{
    PyObject *type_ptr = NULL, *value_ptr = NULL, *traceback_ptr = NULL;
    PyErr_Fetch(&type_ptr, &value_ptr, &traceback_ptr);
    std::string ret("Unfetchable Python error");

    if(type_ptr != NULL){
            py::handle<> h_type(type_ptr);
            py::str type_pstr(h_type);
            py::extract<std::string> e_type_pstr(type_pstr);
            if(e_type_pstr.check())
                ret = e_type_pstr();
            else
                ret = "Unknown exception type";
        }
    if(value_ptr != NULL){
            py::handle<> h_val(value_ptr);
            py::str a(h_val);
            py::extract<std::string> returned(a);
            if(returned.check())
                ret +=  ": " + returned();
            else
                ret += std::string(": Unparseable Python error: ");
        }

    if(traceback_ptr != NULL){
            py::handle<> h_tb(traceback_ptr);
            py::object tb(py::import("traceback"));
            py::object fmt_tb(tb.attr("format_tb"));
            py::object tb_list(fmt_tb(h_tb));
            py::object tb_str(py::str("\n").join(tb_list));
            py::extract<std::string> returned(tb_str);
            if(returned.check())
                ret += ": " + returned();
            else
                ret += std::string(": Unparseable Python traceback");
        }
        return ret;
}
