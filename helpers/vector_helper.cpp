#include "vector_helper.h"

vector<int> vector_helper::left_derivate(const vector<int> &v)
{
    vector<int> d(v.size());
    d[0] = v[0];
    for(unsigned i=1; i<v.size(); i++){
        d[i] = v[i] - v[i-1];
    }

    return d;
}

vector<double> vector_helper::center_derivate(const vector<int> &v)
{
    vector<double> d(v.size());
    d[0] = v[1]/2.0;
    for(unsigned i=1; i<v.size()-1; i++){
        d[i] = (v[i+1]-v[i-1])/2.0;
    }
    d[d.size()-1] = -v[d.size()-2]/2.0;

    return d;
}

vector<int> vector_helper::right_derivate(const vector<int> &v)
{
    vector<int> d(v.size());

    for(unsigned i=0; i<v.size()-1; i++){
        d[i] = v[i+1] - v[i];
    }
    d[d.size()-1] = 0;

    return d;
}

double vector_helper::avg(const vector<int> &v)
{
    return avg(v, Range(0,v.size()));
}

double vector_helper::avg(const vector<int> &v, const Range range)
{
    if(range.start < 0 || range.end > (int) v.size()){
        cerr<<"Error in function avg";
        return 0.0; //TODO: exception
    }

    double value = 0.0;

    for(int i=range.start; i<range.end; i++){
        value += v[i];
    }
    return value / range.size();
}

double vector_helper::expected(const vector<int> &v, Range range)//TODO: can i pass v exactly to argument
{
    if(range == Range(-1,-1)) { /// handling default value
        range.start = 0;
        range.end = v.size();
    }

    double res = 0.0;
    for(int i=range.start; i<range.end; i++){
        res += v[i];
    }
    cerr<<range.size();

    return res/range.size();
}

double vector_helper::expected(const vector<int> &v, function<double (const double)> f, Range range)
{
    if(range == Range(-1,-1)) { /// handling default value
        range.start = 0;
        range.end = v.size();
    }
    double res = 0.0;
    for(int i=range.start; i<range.end; i++){
        res += f(v[i]);
    }
    cerr<<range.size();

    return res/range.size();
}

//TODO: test this function on row with 2 or more runs
vector<Range> vector_helper::getRanges(const vector<int> &v, const int leak_white_range)
{
    vector<Range> ranges;

    Range range(-1,-1);
    bool started = false;
    int leaking_count = 0;
    int temp_end;

    for(uint i=0; i<v.size(); i++){
        int value = v[i];
        if(value > 0) {
            if(!started){
                started = true;
                range.start = i;
                range.end = i+1;
            }
        }else{
            if(started){
                if(leak_white_range == 0){
                    range.end = i-1;
                    ranges.push_back(range);

                    started = false;
                }else{
                    if(leaking_count == 0){
                        temp_end = i-1;
                    }
                    leaking_count++;
                    if(leaking_count >= leak_white_range){
                        leaking_count = 0;
                        range.end = temp_end;
                        started = false;
                    }
                }
            }
        }
    }

    return ranges;
}

double vector_helper::correl(const vector<int> &v1, const vector<int> &v2, int shift)
{
    //TODO: implement

    return 0.0;
}

double vector_helper::dot_prod_shifted(const vector<int> &a, const vector<int> &b, int shift)
{
    int start = 0;
    int end = a.size();
    if(shift < 0){
        end += shift;
    }
    if(shift > 0){
        start += shift;
    }
    double sum = 0;
    for(int i=start; i<end; i++){
        sum += a[i] * b[i - shift];
    }

    return sum;
}
