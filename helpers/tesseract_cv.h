#ifndef TESSERACT_CV_H
#define TESSERACT_CV_H

#include <leptonica/allheaders.h>
#include <tesseract/baseapi.h>
#include <opencv2/opencv.hpp>

#include "helpers/viewer.h"

using namespace std;
using namespace cv;
using namespace tesseract;

class tesseract_cv : public TessBaseAPI
{
    int min_confidence;    //мінімальна ймовірність, за якої знайдена інформація в області вважається текстом
public:
    tesseract_cv(const int min_confidence);

    void SetImage(const Mat &mat);
    void SetRectangle(const Rect &rect);
    vector<Rect> GetRegions();
    string GetUTF8Text();
    int getMinConfidence() const;
    void setMin_confidence(int confidence);
};

#endif // TESSERACT_CV_H
