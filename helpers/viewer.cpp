#include "viewer.h"

int viewer::window_count = 0;

void viewer::viewMat(const Mat &mat, int waitMillis)
{
    string window_title = "window #" + to_string(window_count);

    namedWindow(window_title, WINDOW_NORMAL);
    Mat m = mat.clone();

    imshow(window_title, m);

    cerr<<"viewer: "<<window_title<<endl;
    waitKey(waitMillis);
    window_count++;
}

void viewer::viewMat(string window_title, const Mat &mat, int waitMillis)
{
    namedWindow(window_title, WINDOW_NORMAL);
    Mat m = mat.clone();
    imshow(window_title, m);

    cerr<<"viewer: "<<window_title<<endl;
    waitKey(waitMillis);
}

Mat viewer::viewContours(const Mat &mat, vector <vector <Point>> &contours, const int thickness)
{
    Mat color_mat = binToColorMat(mat);

    Scalar color(255,255,0);

    for(uint i=0; i<contours.size(); i++){
        drawContours(color_mat, contours, i, color, thickness, 4/*, noArray(),INT8_MAX, Point(-1,-1)*/);
    }

    return color_mat;
}

Mat viewer::viewRects(const Mat &source_mat, vector<Rect> rects, const Scalar &rect_color, const int thickness)
{
    Mat color_mat = binToColorMat(source_mat);

    for(Rect r : rects){
        rectangle(color_mat, r, rect_color, thickness);
    }

    return color_mat;
}

Mat viewer::viewLines(const Mat &source_mat, vector<Vec4i> &lines, const Scalar &lines_color,
                      const int thickness,const int line_type, const int shift)
{
    Mat color_mat = binToColorMat(source_mat);

    for(Vec4i l : lines){
        line(color_mat, Point(l[0],l[1]), Point(l[2],l[3]),
                lines_color, thickness, line_type, shift);
    }

    return color_mat;
}

Mat viewer::viewCircles(const Mat &source_mat, vector<Vec3i> &circles, const Scalar &draw_color, const int thickness)
{
    Mat color_mat = binToColorMat(source_mat);

    for(Vec3i c : circles){
        circle(color_mat, Point(c[0],c[1]), c[2], draw_color, thickness);
    }

    return color_mat;
}

Mat viewer::viewPoints(const Mat &source_mat, vector<Point> &points, const Scalar &draw_color,
                       const int point_radius, const int thickness)
{
    Mat color_mat = binToColorMat(source_mat);

    for(Point p : points){
        circle(color_mat, p, point_radius, draw_color, thickness);
    }

    return color_mat;
}

Mat viewer::viewXProjection(const Mat &source_mat, vector<int> x_projection,
                            Rect draw_rect, const Scalar draw_color, bool drawBoundRect)
{
    Mat color_mat = binToColorMat(source_mat);

    uint draw_length = std::min((int)x_projection.size(), draw_rect.width);
    int maxPeak = *max_element(x_projection.begin(), x_projection.begin()+draw_length);
    double heightMultiplier = (double)draw_rect.height/maxPeak;
    int bottomY = draw_rect.br().y;


    for(uint i=0; i<draw_length; i++){
        int x = i + draw_rect.x;
        Point p1(x, bottomY);
        Point p2(x, bottomY - heightMultiplier*x_projection[i]);
        line(color_mat, p1, p2, draw_color);
    }
    if(drawBoundRect){
        rectangle(color_mat, draw_rect, draw_color);
    }
    return color_mat;
}

Mat viewer::viewYProjection(const Mat &source_mat, vector<int> y_projection, Rect draw_rect, const Scalar draw_color, bool drawBoundRect)
{
    Mat color_mat = binToColorMat(source_mat);//TODO: повторить то же, что и для X-проекции.

    int maxPeak = *max_element(y_projection.begin(), y_projection.end());
    double widthMultiplier = (double)draw_rect.width/maxPeak;
    int leftX = draw_rect.x;
    uint draw_length = std::min((int)y_projection.size(), draw_rect.height);

    for(uint i=0; i<draw_length; i++){
        int y = i + draw_rect.y;
        Point p1(leftX, y);
        Point p2(leftX + widthMultiplier*y_projection[i], y);
        line(color_mat, p1, p2, draw_color);
    }
    if(drawBoundRect){
        rectangle(color_mat, draw_rect, draw_color);
    }

    return color_mat;
}



Mat viewer::binToColorMat(const Mat &bin_mat)
{
    Mat color_mat;
    cvtColor(bin_mat, color_mat, CV_GRAY2RGB);
    return color_mat;
}
