#ifndef MAT_HELPER_H
#define MAT_HELPER_H

#include <opencv2/opencv.hpp>
#include <vector>

#include "viewer.h"
#include "my_structs.h"

using namespace std;
using namespace cv;

/**
 * @brief mat_helper - цілком статичний клас, що призначений для виконання різних операцій з зображеннями
 */
class mat_helper
{
public:
    /**
     * @brief save_folder   змінна для визначення папки збереження для функції save_file
     * @see save_file
     */
    static string save_folder;

    /**
     * @brief move_rect     зсуває зображення у зазначеному прямокутнику в зазначеному напрямку
     * @param inputMat      вхідне зображення
     * @param direction     напрямок зсуву (вертикаль, горизонталь)
     * @param rect          прямокутна ділянка зображення для зсуву
     * @param in_value      значення, що замінює "пусті" ділянки після зсуву
     */
    static void move_rect(Mat &inputMat, const Point &direction,
                          const Rect rect, const int in_value = 255);

    static vector<int> y_projection(const Mat &mat, Range x_range, Range y_range);
    /**
     * @brief y_projection  виконання проекції зображення на вісь y
     * @param mat           вхідне зображення
     * @param rect          прямокутна ділянка для проектування
     * @return              вектор-проекція
     */
    static vector<int> y_projection(const Mat &mat, Rect rect);
    static vector<int> x_projection(const Mat &mat, Range x_range, Range y_range);
    /**
     * @brief x_projection  виконання проекції зображення на вісь x
     * @param mat           вхідне зображення
     * @param rect          прямокутна ділянка для проектування
     * @return              вектор-проекція
     */
    static vector<int> x_projection(const Mat &mat, Rect rect);
    /**
     * @brief x_projectionвиконання проекції зображення контура на вісь y
     * @param mat           вхідне зображення
     * @param rect          прямокутна ділянка для проектування
     * @param mask_contour  контур об’єкта для проектування
     * @return              вектор-проекція
     */
    static vector<int> x_projection(const Mat &mat, Rect rect, Contour mask_contour);

    /**
     * @brief create_mask   створює матрицю-маску з колекції прямокутників
     * @param rects         колекція прямокутників
     * @param size          розмір отримуваної маски
     * @param type          тип отримуваної маски
     * @return              матрицю-маску
     */
    static Mat create_mask(const vector<Rect> &rects, const Size &size, const int type);
    /**
     * @brief create_mask   створює матрицю-маску з колекції контурів об’єктів
     * @param contours      колекція контурів
     * @param size          розмір отримуваної маски
     * @param type          тип отримуваної маски
     * @param contour_ldx   індекс контуру в @see contours для рисування маски. Якщо -1, замальовуються всі контури
     * @return              матрицю-маску
     */
    static Mat create_mask(const vector<Contour> &contours, const Size &size, const int type, const int contour_ldx = -1);

    /**
     * @brief save_file виконує збереження зображення до файлу
     * @param mat       зображення для збереження
     * @param filename  ім’я файлу
     * @return          шлях до збереженого файлу
     */
    static string save_file(const Mat &mat, const string &filename);
};

#endif // MAT_HELPER_H
