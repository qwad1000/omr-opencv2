#ifndef VIEWER_H
#define VIEWER_H

#include <opencv2/opencv.hpp>
#include <algorithm>

using namespace std;
using namespace cv;

class viewer
{
    static int window_count;
public:
    static void viewMat(const Mat& mat, int waitMillis = 0);
    static void viewMat(string window_title, const Mat& mat, int waitMillis = 0);

    static Mat viewContours(const Mat &mat, vector<vector<Point> > &contours, const int thickness = 1);
    static Mat viewRects(const Mat &source_mat, vector<Rect> rects,
                         const Scalar &rect_color = Scalar(255,0,0), const int thickness = 1);
    static Mat viewLines(const Mat &source_mat, vector<Vec4i> &lines,
                         const Scalar &lines_color = Scalar(255,0,0), const int thickness = 1,
                         const int line_type = 8, const int shift = 0);
    static Mat viewCircles(const Mat &source_mat, vector<Vec3i> &circles,
                           const Scalar &draw_color = Scalar(255,0,0), const int thickness = 1);
    static Mat viewPoints(const Mat &source_mat, vector<Point> &points,
                          const Scalar &draw_color = Scalar(255,0,0),
                          const int point_radius=3, const int thickness = -1);

    static Mat viewXProjection(const Mat &source_mat, vector<int> x_projection,
                               Rect draw_rect, const Scalar draw_color = Scalar(255,0,0),
                               bool drawBoundRect = true);

    static Mat viewYProjection(const Mat &source_mat, vector<int> y_projection,
                               Rect draw_rect, const Scalar draw_color = Scalar(255,0,0),
                               bool drawBoundRect = true);

    static Mat binToColorMat(const Mat &bin_mat);
};

#endif // VIEWER_H
