#include "runlength.h"

vector<int> runlength::to_rle(const Mat &mat)
{
    vector<int> rle;
    MatConstIterator_<uchar>
            it = mat.begin<uchar>(),
            end = mat.end<uchar>(),
            next;

    while(it != end){
        next = run_end(it, end, true);
        rle.push_back(next - it);
        it = next;

        next = run_end(it, end, false);
        rle.push_back(next - it);
        it = next;
    }

    return rle;
}

Mat runlength::from_rle(const Size mat_size, const vector<int> &rle)
{
    Mat mat = Mat::zeros(mat_size, CV_8UC1);

    uchar *ptr = mat.ptr();
    uchar current_color = 255;
    for(uint i=0; i<rle.size(); i++){
        fill(ptr, ptr+rle[i], current_color);
        ptr += rle[i];
        current_color = (current_color == 0) ? 255 : 0;
    }

    return mat;
}

Mat runlength::from_rle(const Size mat_size, const string &rle)
{
    boost::char_separator<char> sep(" ");
    boost::tokenizer<boost::char_separator<char>> tokens(rle, sep);

    vector<int> rle_int;
    for(std::string str : tokens){
        rle_int.push_back(stoi(str));
    }

    return from_rle(mat_size, rle_int);
}

void runlength::filter_vertical_run(Mat &m, int length, int column, const function<bool(int, int)> &functor, bool color)
{
    int i=0;
    while(i<m.rows){
        if((m.at<uchar>(i,column)>0) == color){
            int f = run_end_vertical(m,i,m.rows,column);
            if(functor(f-i,length)){
                fill_verctical_run(m,i,f,column,!color);
            }
            i = f;
        }else{
            i = run_end_vertical(m,i,m.rows,column);
        }
    }
}

void runlength::filter_vertical_runs(Mat &m, int length, const function<bool (int, int)> &functor, bool color)
{
    for(int i=0; i<m.cols; i++){
        filter_vertical_run(m, length, i, functor, color);
    }
}

void runlength::filter_vertical_short_runs(Mat &m, int length, bool whitecolor)
{
    filter_vertical_runs(m, length, std::less<int>(), whitecolor);
}

void runlength::filter_vertical_long_runs(Mat &m, int length, bool whitecolor)
{
    filter_vertical_runs(m, length, std::greater<int>(), whitecolor);
}

int runlength::find_max_vertical_run(Mat &m, int column, bool color)
{
    int i=0;
    int max = 0;
    while(i<m.rows){
        int f = run_end_vertical(m, i, m.rows, column);
        if(f-i > max && (m.at<uchar>(i,column)>0)==color){
            max = f-i;
        }
        i = f;
    }
    return max;
}

int runlength::find_max_vertical_run(Mat &m, bool color)
{
    int max = 0;
    for(int col=0; col<m.cols; col++){
        int res = find_max_vertical_run(m, col, color);
        if(res > max){
            max = res;
        }
    }
    return max;
}

void runlength::filter_horizontal_run(Mat &m, int length, int row, const function<bool(int, int)> &functor, bool color)
{
    int i=0;
    while(i<m.cols){
        if((m.at<uchar>(row,i)>0) == color){
            int f = run_end_horizontal(m, i, m.cols, row);
            if(functor(f-i, length)){
                fill_horizontal_run(m, i, f, row, !color);
            }
            i=f;
        }else{
            i = run_end_horizontal(m, i, m.cols, row);
        }
    }
}

void runlength::filter_horizontal_runs(Mat &m, int length, const function<bool (int, int)> &functor, bool color)
{
    for(int i=0; i<m.rows; i++){
        filter_horizontal_run(m, length, i, functor, color);
    }
}

void runlength::filter_horizontal_short_runs(Mat &m, int length, bool whitecolor)
{
    filter_horizontal_runs(m, length, std::less<int>(), whitecolor);
}

void runlength::filter_horizontal_long_runs(Mat &m, int length, bool whitecolor)
{
    filter_horizontal_runs(m, length, std::greater<int>(), whitecolor);
}

int runlength::run_end_vertical(const Mat &m, int rstart, int rend, int col)
{
    bool current_color = (m.at<uchar>(rstart, col) > 0);
    int i;
    for(i=rstart+1; i<rend; i++){
        if((m.at<uchar>(i,col)>0) != current_color){
            break;
        }
    }
    return i;
}

void runlength::fill_verctical_run(Mat &m, int rstart, int rend, int col, bool whitecolor)
{
    uchar color = whitecolor ? 255 : 0;
    for(int i=rstart; i<rend; i++){
        m.at<uchar>(i, col) = color;
    }
}

int runlength::run_end_horizontal(const Mat &m, int cstart, int cend, int row)
{
    bool current_color = (m.at<uchar>(row, cstart) > 0);
    int i;
    for(i=cstart+1; i<cend; i++){
        if((m.at<uchar>(row,i)>0) != current_color){
            break;
        }
    }
    return i;
}

void runlength::fill_horizontal_run(Mat &m, int cstart, int cend, int row, bool whitecolor)
{
    uchar color = whitecolor ? 255 : 0;
    for(int i=cstart; i<cend; i++){
        m.at<uchar>(row,i) = color;
    }
}

MatConstIterator_<uchar> runlength::run_end(MatConstIterator_<uchar> i, MatConstIterator_<uchar> end, bool color)
{
    for(;i!=end; i++){
        if((*i > 0) != color){
            break;
        }
    }
    return i;
}
