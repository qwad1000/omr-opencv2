#include "mat_helper.h"

//TODO: realize xor??
void mat_helper::move_rect(Mat &inputMat, const Point &direction, const Rect rect, const int in_value)
{
    if(direction == Point(0,0)){
        return ;
    }
    Rect shiftedRect = rect + direction;
    Rect imageRect(0,0,inputMat.cols, inputMat.rows);
    Rect insideRect = shiftedRect & imageRect;

    int irowstart = (direction.y < 0) ? insideRect.tl().y : insideRect.br().y-1,
            irowend = (direction.y < 0) ? insideRect.br().y-1 : insideRect.tl().y ,
            jcolstart = (direction.x < 0) ? insideRect.tl().x : insideRect.br().x-1,
            jcolend = (direction.x < 0) ? insideRect.br().x-1 : insideRect.tl().x;

    int irow = irowstart;

    while(true){
        int jcol = jcolstart;

        while(true){
            int value = inputMat.at<uchar>(irow - direction.y,jcol - direction.x);
            inputMat.at<uchar>(irow, jcol) = value;

            if(irow - direction.y < 0 || irow - direction.y >= inputMat.rows){
                throw "first throw";
            }
            if(jcol - direction.x < 0 || jcol - direction.x >=inputMat.cols){
                throw "first throw1";
            }

            if(irow < 0 || irow >= inputMat.rows){
                throw "second throw";
            }
            if(jcol < 0 || jcol >=inputMat.cols){
                throw "second throw1";
            }

            if(in_value >= 0){
                Point p(jcol - direction.x,irow - direction.y);
                if(!p.inside(insideRect)){
                    //old_row[jcol - direction.x] = (uchar) in_value; //TODO:!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    inputMat.at<uchar>(irow - direction.y,jcol - direction.x) = (uchar) in_value;
                }
            }
            if(direction.x < 0){
                jcol++;
                if(jcol > jcolend){
                    break;
                }
            }else{
                jcol--;
                if(jcol < jcolend){
                    break;
                }
            }
        }

        if(direction.y < 0){
            irow++;
            if(irow > irowend){
                break;
            }
        }else{
            irow--;
            if(irow < irowend){
                break;
            }
        }
    }
}

vector<int> mat_helper::y_projection(const Mat &mat, Range x_range, Range y_range)
{
    vector <int> y_proj(y_range.size());
    for(int irow=y_range.start; irow<y_range.end; irow++){
        int sum = 0;
        for(int jcol=x_range.start; jcol<x_range.end; jcol++){
            int pixel = mat.at<uchar>(irow, jcol);
            if(pixel == 0){
                //if(irow)
                sum++;
            }
        }
        y_proj[irow - y_range.start] = sum;
    }

    return y_proj;
}

vector<int> mat_helper::y_projection(const Mat &mat, Rect rect)
{
    Range x_range(rect.x, rect.x + rect.width);
    Range y_range(rect.y, rect.y + rect.height);
    return y_projection(mat, x_range, y_range);
}

vector<int> mat_helper::x_projection(const Mat &mat, Range x_range, Range y_range)
{
    vector <int> x_proj(x_range.size());
    for(int jcol=x_range.start; jcol<x_range.end; jcol++){
        int sum = 0;
        for(int irow=y_range.start; irow<y_range.end; irow++){
            int pixel = mat.at<uchar>(irow,jcol);
            if(pixel == 0){
                sum++;
            }
        }
        x_proj[jcol - x_range.start] = sum;
    }

    return x_proj;
}

vector<int> mat_helper::x_projection(const Mat &mat, Rect rect)
{
    Range x_range(rect.x, rect.x + rect.width);
    Range y_range(rect.y, rect.y + rect.height);
    return x_projection(mat, x_range, y_range);
}

vector<int> mat_helper::x_projection(const Mat &mat, Rect rect, Contour mask_contour)
{
    Mat m = Mat::zeros(mat.rows, mat.cols, mat.type());
    bitwise_not(m,m);
    vector<Contour> con_vec(1,mask_contour);
    drawContours(m,con_vec, -1,Scalar(0),-1);
    return x_projection(m,rect);
}
//TODO: проверить работу такой маски. Возможно, нужно инвертировать ее.
Mat mat_helper::create_mask(const vector<Rect> &rects, const Size &size, const int type)
{
    Mat mask = Mat::zeros(size, type);
    for(auto r : rects){
        rectangle(mask, r, Scalar(255), -1);
    }

    return mask;
}

Mat mat_helper::create_mask(const vector<Contour> &contours, const Size &size, const int type, const int contour_ldx)
{
    Mat mask = Mat::zeros(size, type);
    drawContours(mask, contours, contour_ldx, Scalar(255), -1);
    return mask;
}

string mat_helper::save_folder = "";

string mat_helper::save_file(const Mat &mat, const string &filename)
{
    try {
        string path = save_folder + filename;
        imwrite(path, mat);
        cout<<"image saved into: "<<path<<endl;

        return path;
    }
    catch (runtime_error& ex) {
        cerr <<  "Exception saving image:" << ex.what();
    }
    return "";
}

