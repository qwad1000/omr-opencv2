#ifndef COLOR_HELPER_H
#define COLOR_HELPER_H

#include <opencv2/opencv.hpp>

enum class cv_color {
    //Red colors
    lightsalmon, salmon, darksalmon, lightcoral, indianred, crimson, firebrick, red, darkred,
    //Orange colors
    coral, tomato, orangered, gold, orange, darkorange,
    //Yellow colors
    lightyellow, lemonchiffon, lightgoldenrodyellow, papayawhip, moccasin, peachpuff, palegoldenrod, khaki, darkkhaki, yellow,
    //Green colors
    lawngreen, chartreuse, limegreen, lime, forestgreen, green, darkgreen, greenyellow, yellowgreen, springgreen, mediumspringgreen, lightgreen, palegreen, darkseagreen, mediumseagreen, seagreen, olive, darkolivegreen, olivedrab,
    //Cyan colors
    lightcyan, cyan, aqua, aquamarine, mediumaquamarine, paleturquoise, turquoise, mediumturquoise, darkturquoise, lightseagreen, cadetblue, darkcyan, teal,
    //Blue colors
    powderblue, lightblue, lightskyblue, skyblue, deepskyblue, lightsteelblue, dodgerblue, cornflowerblue, steelblue, royalblue, blue, mediumblue, darkblue, navy, midnightblue, mediumslateblue, slateblue, darkslateblue,
    //Purple colors
    lavender, thistle, plum, violet, orchid, fuchsia, magenta, mediumorchid, mediumpurple, blueviolet, darkviolet, darkorchid, darkmagenta, purple, indigo,
    //Pink colors
    pink, lightpink, hotpink, deeppink, palevioletred, mediumvioletred,
    //White colors
    white, snow, honeydew, mintcream, azure, aliceblue, ghostwhite, whitesmoke, seashell, beige, oldlace, floralwhite, ivory, antiquewhite, linen, lavenderblush, mistyrose,
    //Gray colors
    gainsboro, lightgray, silver, darkgray, gray, dimgray, lightslategray, slategray, darkslategray, black,
    //Brown colors
    cornsilk, blanchedalmond, bisque, navajowhite, wheat, burlywood, tan, rosybrown, sandybrown, goldenrod, peru, chocolate, saddlebrown, sienna, brown,   	maroon,
};

enum class cv_color_type {
    rgb,
    bgr
};

struct my_color{
    int red, green, blue;
    my_color(int a_red, int a_green,int a_blue)
        : red(a_red),
          green(a_green),
          blue(a_blue)
    {}
};

class color_helper
{
public:
    static cv::Scalar getColorScalar(cv_color colorName, cv_color_type type = cv_color_type::bgr)
    {
        my_color color = getColor(colorName);
        switch (type) {
        case cv_color_type::bgr:
            return cv::Scalar(color.blue, color.green, color.red);
        case cv_color_type::rgb:
            return cv::Scalar(color.red, color.green, color.blue);
        }
        return cv::Scalar(0,0,0);
    }

    static my_color getColor(cv_color color){
        switch (color){
        //([a-z]+)\s+#([\d|\w]{6})\s+rgb\(([\d]+),([\d]+),([\d]+)\)
        //http://www.rapidtables.com/web/color/html-color-codes.htm
        //Red colors
        case cv_color::lightsalmon:	return my_color(255,160,122); //hex(FFA07A)
            case cv_color::salmon:	return my_color(250,128,114); //hex(FA8072)
            case cv_color::darksalmon:	return my_color(233,150,122); //hex(E9967A)
            case cv_color::lightcoral:	return my_color(240,128,128); //hex(F08080)
            case cv_color::indianred:	return my_color(205,92,92); //hex(CD5C5C)
            case cv_color::crimson:	return my_color(220,20,60); //hex(DC143C)
            case cv_color::firebrick:	return my_color(178,34,34); //hex(B22222)
            case cv_color::red:	return my_color(255,0,0); //hex(FF0000)
            case cv_color::darkred:	return my_color(139,0,0); //hex(8B0000)

        //Orange colors
            case cv_color::coral:	return my_color(255,127,80); //hex(FF7F50)
            case cv_color::tomato:	return my_color(255,99,71); //hex(FF6347)
            case cv_color::orangered:	return my_color(255,69,0); //hex(FF4500)
            case cv_color::gold:	return my_color(255,215,0); //hex(FFD700)
            case cv_color::orange:	return my_color(255,165,0); //hex(FFA500)
            case cv_color::darkorange:	return my_color(255,140,0); //hex(FF8C00)

        //Yellow colors
            case cv_color::lightyellow:	return my_color(255,255,224); //hex(FFFFE0)
            case cv_color::lemonchiffon:	return my_color(255,250,205); //hex(FFFACD)
            case cv_color::lightgoldenrodyellow:	return my_color(250,250,210); //hex(FAFAD2)
            case cv_color::papayawhip:	return my_color(255,239,213); //hex(FFEFD5)
            case cv_color::moccasin:	return my_color(255,228,181); //hex(FFE4B5)
            case cv_color::peachpuff:	return my_color(255,218,185); //hex(FFDAB9)
            case cv_color::palegoldenrod:	return my_color(238,232,170); //hex(EEE8AA)
            case cv_color::khaki:	return my_color(240,230,140); //hex(F0E68C)
            case cv_color::darkkhaki:	return my_color(189,183,107); //hex(BDB76B)
            case cv_color::yellow:	return my_color(255,255,0); //hex(FFFF00)

        //Green colors
            case cv_color::lawngreen:	return my_color(124,252,0); //hex(7CFC00)
            case cv_color::chartreuse:	return my_color(127,255,0); //hex(7FFF00)
            case cv_color::limegreen:	return my_color(50,205,50); //hex(32CD32)
            case cv_color::lime:	return my_color(0,255,0); //hex(00FF00)
            case cv_color::forestgreen:	return my_color(34,139,34); //hex(228B22)
            case cv_color::green:	return my_color(0,128,0); //hex(008000)
            case cv_color::darkgreen:	return my_color(0,100,0); //hex(006400)
            case cv_color::greenyellow:	return my_color(173,255,47); //hex(ADFF2F)
            case cv_color::yellowgreen:	return my_color(154,205,50); //hex(9ACD32)
            case cv_color::springgreen:	return my_color(0,255,127); //hex(00FF7F)
            case cv_color::mediumspringgreen:	return my_color(0,250,154); //hex(00FA9A)
            case cv_color::lightgreen:	return my_color(144,238,144); //hex(90EE90)
            case cv_color::palegreen:	return my_color(152,251,152); //hex(98FB98)
            case cv_color::darkseagreen:	return my_color(143,188,143); //hex(8FBC8F)
            case cv_color::mediumseagreen:	return my_color(60,179,113); //hex(3CB371)
            case cv_color::seagreen:	return my_color(46,139,87); //hex(2E8B57)
            case cv_color::olive:	return my_color(128,128,0); //hex(808000)
            case cv_color::darkolivegreen:	return my_color(85,107,47); //hex(556B2F)
            case cv_color::olivedrab:	return my_color(107,142,35); //hex(6B8E23)

        //Cyan colors
            case cv_color::lightcyan:	return my_color(224,255,255); //hex(E0FFFF)
            case cv_color::cyan:	return my_color(0,255,255); //hex(00FFFF)
            case cv_color::aqua:	return my_color(0,255,255); //hex(00FFFF)
            case cv_color::aquamarine:	return my_color(127,255,212); //hex(7FFFD4)
            case cv_color::mediumaquamarine:	return my_color(102,205,170); //hex(66CDAA)
            case cv_color::paleturquoise:	return my_color(175,238,238); //hex(AFEEEE)
            case cv_color::turquoise:	return my_color(64,224,208); //hex(40E0D0)
            case cv_color::mediumturquoise:	return my_color(72,209,204); //hex(48D1CC)
            case cv_color::darkturquoise:	return my_color(0,206,209); //hex(00CED1)
            case cv_color::lightseagreen:	return my_color(32,178,170); //hex(20B2AA)
            case cv_color::cadetblue:	return my_color(95,158,160); //hex(5F9EA0)
            case cv_color::darkcyan:	return my_color(0,139,139); //hex(008B8B)
            case cv_color::teal:	return my_color(0,128,128); //hex(008080)

        //Blue colors
            case cv_color::powderblue:	return my_color(176,224,230); //hex(B0E0E6)
            case cv_color::lightblue:	return my_color(173,216,230); //hex(ADD8E6)
            case cv_color::lightskyblue:	return my_color(135,206,250); //hex(87CEFA)
            case cv_color::skyblue:	return my_color(135,206,235); //hex(87CEEB)
            case cv_color::deepskyblue:	return my_color(0,191,255); //hex(00BFFF)
            case cv_color::lightsteelblue:	return my_color(176,196,222); //hex(B0C4DE)
            case cv_color::dodgerblue:	return my_color(30,144,255); //hex(1E90FF)
            case cv_color::cornflowerblue:	return my_color(100,149,237); //hex(6495ED)
            case cv_color::steelblue:	return my_color(70,130,180); //hex(4682B4)
            case cv_color::royalblue:	return my_color(65,105,225); //hex(4169E1)
            case cv_color::blue:	return my_color(0,0,255); //hex(0000FF)
            case cv_color::mediumblue:	return my_color(0,0,205); //hex(0000CD)
            case cv_color::darkblue:	return my_color(0,0,139); //hex(00008B)
            case cv_color::navy:	return my_color(0,0,128); //hex(000080)
            case cv_color::midnightblue:	return my_color(25,25,112); //hex(191970)
            case cv_color::mediumslateblue:	return my_color(123,104,238); //hex(7B68EE)
            case cv_color::slateblue:	return my_color(106,90,205); //hex(6A5ACD)
            case cv_color::darkslateblue:	return my_color(72,61,139); //hex(483D8B)

        //Purple colors
            case cv_color::lavender:	return my_color(230,230,250); //hex(E6E6FA)
            case cv_color::thistle:	return my_color(216,191,216); //hex(D8BFD8)
            case cv_color::plum:	return my_color(221,160,221); //hex(DDA0DD)
            case cv_color::violet:	return my_color(238,130,238); //hex(EE82EE)
            case cv_color::orchid:	return my_color(218,112,214); //hex(DA70D6)
            case cv_color::fuchsia:	return my_color(255,0,255); //hex(FF00FF)
            case cv_color::magenta:	return my_color(255,0,255); //hex(FF00FF)
            case cv_color::mediumorchid:	return my_color(186,85,211); //hex(BA55D3)
            case cv_color::mediumpurple:	return my_color(147,112,219); //hex(9370DB)
            case cv_color::blueviolet:	return my_color(138,43,226); //hex(8A2BE2)
            case cv_color::darkviolet:	return my_color(148,0,211); //hex(9400D3)
            case cv_color::darkorchid:	return my_color(153,50,204); //hex(9932CC)
            case cv_color::darkmagenta:	return my_color(139,0,139); //hex(8B008B)
            case cv_color::purple:	return my_color(128,0,128); //hex(800080)
            case cv_color::indigo:	return my_color(75,0,130); //hex(4B0082)

        //Pink colors
            case cv_color::pink:	return my_color(255,192,203); //hex(FFC0CB)
            case cv_color::lightpink:	return my_color(255,182,193); //hex(FFB6C1)
            case cv_color::hotpink:	return my_color(255,105,180); //hex(FF69B4)
            case cv_color::deeppink:	return my_color(255,20,147); //hex(FF1493)
            case cv_color::palevioletred:	return my_color(219,112,147); //hex(DB7093)
            case cv_color::mediumvioletred:	return my_color(199,21,133); //hex(C71585)

        //White colors
            case cv_color::white:	return my_color(255,255,255); //hex(FFFFFF)
            case cv_color::snow:	return my_color(255,250,250); //hex(FFFAFA)
            case cv_color::honeydew:	return my_color(240,255,240); //hex(F0FFF0)
            case cv_color::mintcream:	return my_color(245,255,250); //hex(F5FFFA)
            case cv_color::azure:	return my_color(240,255,255); //hex(F0FFFF)
            case cv_color::aliceblue:	return my_color(240,248,255); //hex(F0F8FF)
            case cv_color::ghostwhite:	return my_color(248,248,255); //hex(F8F8FF)
            case cv_color::whitesmoke:	return my_color(245,245,245); //hex(F5F5F5)
            case cv_color::seashell:	return my_color(255,245,238); //hex(FFF5EE)
            case cv_color::beige:	return my_color(245,245,220); //hex(F5F5DC)
            case cv_color::oldlace:	return my_color(253,245,230); //hex(FDF5E6)
            case cv_color::floralwhite:	return my_color(255,250,240); //hex(FFFAF0)
            case cv_color::ivory:	return my_color(255,255,240); //hex(FFFFF0)
            case cv_color::antiquewhite:	return my_color(250,235,215); //hex(FAEBD7)
            case cv_color::linen:	return my_color(250,240,230); //hex(FAF0E6)
            case cv_color::lavenderblush:	return my_color(255,240,245); //hex(FFF0F5)
            case cv_color::mistyrose:	return my_color(255,228,225); //hex(FFE4E1)

        //Gray colors
            case cv_color::gainsboro:	return my_color(220,220,220); //hex(DCDCDC)
            case cv_color::lightgray:	return my_color(211,211,211); //hex(D3D3D3)
            case cv_color::silver:	return my_color(192,192,192); //hex(C0C0C0)
            case cv_color::darkgray:	return my_color(169,169,169); //hex(A9A9A9)
            case cv_color::gray:	return my_color(128,128,128); //hex(808080)
            case cv_color::dimgray:	return my_color(105,105,105); //hex(696969)
            case cv_color::lightslategray:	return my_color(119,136,153); //hex(778899)
            case cv_color::slategray:	return my_color(112,128,144); //hex(708090)
            case cv_color::darkslategray:	return my_color(47,79,79); //hex(2F4F4F)
            case cv_color::black:	return my_color(0,0,0); //hex(000000)

        //Brown colors
            case cv_color::cornsilk:	return my_color(255,248,220); //hex(FFF8DC)
            case cv_color::blanchedalmond:	return my_color(255,235,205); //hex(FFEBCD)
            case cv_color::bisque:	return my_color(255,228,196); //hex(FFE4C4)
            case cv_color::navajowhite:	return my_color(255,222,173); //hex(FFDEAD)
            case cv_color::wheat:	return my_color(245,222,179); //hex(F5DEB3)
            case cv_color::burlywood:	return my_color(222,184,135); //hex(DEB887)
            case cv_color::tan:	return my_color(210,180,140); //hex(D2B48C)
            case cv_color::rosybrown:	return my_color(188,143,143); //hex(BC8F8F)
            case cv_color::sandybrown:	return my_color(244,164,96); //hex(F4A460)
            case cv_color::goldenrod:	return my_color(218,165,32); //hex(DAA520)
            case cv_color::peru:	return my_color(205,133,63); //hex(CD853F)
            case cv_color::chocolate:	return my_color(210,105,30); //hex(D2691E)
            case cv_color::saddlebrown:	return my_color(139,69,19); //hex(8B4513)
            case cv_color::sienna:	return my_color(160,82,45); //hex(A0522D)
            case cv_color::brown:	return my_color(165,42,42); //hex(A52A2A)
            case cv_color::maroon:	return my_color(128,0,0); //hex(800000)
        }
        return my_color(0,0,0);
    }

};

#endif // COLOR_HELPER_H
