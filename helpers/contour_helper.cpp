#include "contour_helper.h"

Range contour_helper::getXRange(vector<Point> contour)
{
    if(contour.size() == 0){
        throw new string("contour is empty");
    }
    Range range(contour[0].x, contour[0].x); //WARNING: возможен баг в inRange
    for(uint i=1; i<contour.size(); i++){
        Point p = contour[i];
        int res = inRange(range, p.x);
        if(res < 0){
            range.start = p.x;
        }
        if(res > 0){
            range.start = p.x;
        }
    }
    return range;
}

Range contour_helper::getYRange(vector<Point> contour)
{
    if(contour.size() == 0){
        throw new string("contour is empty");
    }

    Range range(contour[0].y, contour[0].y); //WARNING: возможен баг в inRange
    for(uint i=1; i<contour.size(); i++){
        Point p = contour[i];
        int res = inRange(range, p.y);
        if(res < 0){
            range.start = p.y;
        }
        if(res > 0){
            range.start = p.y;
        }
    }
    return range;
}

vector<Contour> contour_helper::findExternalContours(const Mat &image)
{
    Mat m = image.clone();
    bitwise_not(m, m);

    vector<Contour> contours;
    findContours(m, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);

    return contours;
}

vector<Rect> contour_helper::boundingRectangles(const vector<Contour> &contours)
{
    vector<Rect> rects(contours.size());

    for(uint i=0; i<contours.size(); i++){
        rects[i] = boundingRect(contours[i]);
    }

    return rects;
}

int contour_helper::inRange(const Range &r, const int value)
{
    if(value >= r.start && value < r.end){
        return 0;
    }
    if(value < r.start){
        return -1;
    }

    return 1;
}
