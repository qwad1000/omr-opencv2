#ifndef MY_ANALYSER_H
#define MY_ANALYSER_H

#include <stdio.h>
#include <vector>
#include <algorithm>
#include <opencv2/opencv.hpp>

#include "ocr_performer.h"
#include "my_structs.h"
#include "deskewer.h"
#include "helpers/viewer.h"
#include "mask.h"
#include "helpers/vector_helper.h"
#include "row_performer.h"
#include "obj_performer.h"
#include "helpers/py_classifier.h"


using namespace cv;
using namespace std;

/**
 * @brief my_analyser - клас, що інкапсулює роботу з одним зображенням. Саме тут виконується вся робота та управління іншими інструментами
 */
class main_analyser
{
    Mat _source_image;
    tesseract_cv &_tess;
    py_classifier &_gameraClassifier;

    Mat _objMat;

public:
    /**
     * @brief my_analyser єдиний конструктор класу
     * @param source_image_mat  матриця вихідного зображення в бінарному вигляді
     * @param tess              посилання на об’єкт ocr аналізатора
     * @param gameraClassifier  посилання на об’єкт knn класифікатора
     */
    main_analyser(const Mat &source_image_mat, tesseract_cv &tess, py_classifier &gameraClassifier);
    ~main_analyser();

    /**
     * @brief perform головна функція класу.
     */
    void perform();

    Mat getObjMat() const {
        return _objMat;
    }

private:
    /**
     * @brief clearImageBounds очищення крайових пікселів зображення
     * @param image             вхідне зображення
     * @param boundWidth        ширина смуг для видалення
     */
    static void clearImageBounds(Mat &image, int boundWidth = 10);

    /**
     * @brief calculate_staff_properties виконує підрахунок основних властивостей нотного стану
     * @param image вхідне зображення
     * @return структура з двома показниками: висоти ліній та проміжків між ними.
     */
    staff_properties calculate_staff_properties(const Mat &image);

    /**
     * @brief deleteStuff функція для видалення з зображення вертикальних послідовностей піклелів, що більші за deleteHeight
     * @param[in|out] originalMat матриця зображення для роботи
     * @param deleteHeight мінімальна висота послідовності для видалення
     */
    static void deleteStuff(const Mat &originalMat, const int deleteHeight);

    /**
     * @brief find_tall_objects знаходить на вхідному зображенні відносно високі об’єкти і видаляє їх з зображення
     * @param [in|out] destuffedskewed_mat зображення з об’єктами
     * @param prop властивості нотного стану.
     * @return вектор контурів знайдених відносно високих об’єктів
     */
    static vector<Contour> find_tall_objects(Mat &destuffedskewed_mat, staff_properties prop);
};

#endif // MY_ANALYSER_H
